<?php


namespace app\controllers\rrapi;

use app\components\behaviors\Response;
use app\models\forms\{OrderForm, OrderInfoForm, OrderPhotosForm, StatusForm};
use Yii;

class OrderController extends BaseController
{
    public function actionCreateOrder()
    {
        try {
            if (Yii::$app->getRequest()->isPost) {
                $model = new OrderForm();
                if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
                    if ($model->validate()) {
                        if ($model->parseAddress()) {
                            $model->agency_id = $this->agency_id;
                            return $model->createOrder();
                        }
                        return Response::responseError('Не удалось определить адрес по этим координатам: ' . $model->coordinates);
                    } else {
                        return Response::validateErrors($model->errors);
                    }
                }
                return Response::responseError('Данные в запросе отсутсвуют');
            }
            return Response::methodNotAllowed('POST');
        } catch (\Exception $e) {
            return Response::errorOnSave($e->getMessage());
        }
    }

    public function actionGetStatus($id)
    {
        try {
            if (Yii::$app->getRequest()->isGet) {
                $model = new StatusForm();
                $model->rrId = $id;
                if ($model->validate()) {
                    return $model->getStatus();
                }
                return Response::validateErrors($model->errors);
            }
            return Response::methodNotAllowed('GET');
        } catch (\Exception $e) {
            return Response::errorOnSave($e->getMessage());
        }
    }

    public function actionGetOrder($id)
    {
        try {
            if (Yii::$app->getRequest()->isGet) {
                $model = new OrderInfoForm();
                $model->rrId = $id;
                if ($model->validate()) {
                    return $model->getOrder();
                }
                return Response::validateErrors($model->errors);
            }
            return Response::methodNotAllowed('GET');
        } catch (\Exception $e) {
            return Response::errorOnSave($e->getMessage());
        }
    }

    public function actionGetPhotos($id)
    {
        try {
            if (Yii::$app->getRequest()->isGet) {
                $model = new OrderPhotosForm();
                $model->rrId = $id;
                if ($model->validate()) {
                    return $model->getPhotos();
                }
                return Response::validateErrors($model->errors);
            }
            return Response::methodNotAllowed('GET');
        } catch (\Exception $e) {
            return Response::errorOnSave($e->getMessage());
        }
    }
}