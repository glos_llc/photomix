<?php


namespace app\controllers\rrapi;


use app\models\Agency;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\Controller;

class BaseController extends Controller
{
    public $agency_id;

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->enableAutoLogin = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                'Access-Control-Request-Method' => ['POST', 'GET'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['Authorization', '*'],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::class,
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        unset($behaviors['authenticator']);
        $behaviors['authenticator']['class'] = HttpBasicAuth::class;
        $behaviors['authenticator']['auth'] = function ($username, $password) {
            $user = \app\models\Identity::findOne([
                'login' => $username,
            ]);
            if ($user) {
                if (\Yii::$app->security->validatePassword($password, $user->password)){
                    $this->agency_id = Agency::findOne(['identity' => $user->id])->id;
                    return $user;
                }else{
                    return null;
                }
            }
            return null;
        };
        return $behaviors;
    }
}