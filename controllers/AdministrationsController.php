<?php

namespace app\controllers;

use Yii;
use app\models\{Constant, Administrations, Identity, search\AdministrationsSearch, User};
use yii\filters\{AccessControl, VerbFilter};
use yii\web\{NotFoundHttpException, Controller};

/**
 * AdministrationsController implements the CRUD actions for Administrations model.
 */
class AdministrationsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Administrations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdministrationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Administrations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Administrations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $identity = new Identity();
        $model = new Administrations(['identity' => $identity->id]);

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $identity->load(Yii::$app->request->post()) &&
            $identity->save() && $model->save()) {

            Yii::$app->authManager->assign(Yii::$app->authManager->getRole(Constant::ROLE_ADMINISTRATION), $identity->id);
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'identity' => $identity
        ]);
    }

    /**
     * Updates an existing Administrations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $identity = $model->entity;

        if ($model->load(Yii::$app->request->post()) && $model->entity->load(Yii::$app->request->post()) &&
            $model->entity->save() && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'identity' => $identity
        ]);
    }

    /**
     * Deletes an existing Administrations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        Yii::$app->authManager->revokeAll($model->entity->id);
        $model->entity->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Administrations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Administrations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Administrations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
