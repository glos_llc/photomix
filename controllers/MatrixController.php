<?php

namespace app\controllers;

use app\components\interfaces\Access;
use Yii;
use app\models\{Administrations, Constant, Matrix, search\MatrixSearch};
use yii\filters\{AccessControl, VerbFilter};
use yii\web\{Controller, HttpException, NotFoundHttpException};

/**
 * MatrixController implements the CRUD actions for Matrix model.
 */
class MatrixController extends Controller implements Access
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN, Constant::ROLE_ADMINISTRATION],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Matrix models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatrixSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Matrix model.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionView($id)
    {
        $this->checkAccess($id);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Matrix model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Matrix();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Matrix model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $this->checkAccess($id);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Matrix model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->checkAccess($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Matrix model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Matrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matrix::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function checkAccess($id)
    {
        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            $model = $this->findModel($id);

            if (is_null($model->photographer_id)) {
                $administration_id = Matrix::find()->select(['matrix.*', 'real_estate_agency.*'])
                    ->leftJoin('real_estate_agency', 'matrix.agency_id = real_estate_agency.id')
                    ->where(['matrix.id' => $id])->asArray()->one();

                if ($administration_id['administration_id'] != Administrations::findOne(['identity' => Yii::$app->user->id])->id) {
                    throw new HttpException(403, 'Forbidden');
                }
            } else {
                $administration_id = Matrix::find()->select(['matrix.*', 'photographer.*'])
                    ->leftJoin('photographer', 'matrix.photographer_id = photographer.id')
                    ->where(['matrix.id' => $id])->asArray()->one();

                if ($administration_id['administration_id'] != Administrations::findOne(['identity' => Yii::$app->user->id])->id) {
                    throw new HttpException(403, 'Forbidden');
                }
            }
        }
    }
}
