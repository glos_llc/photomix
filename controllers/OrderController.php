<?php

namespace app\controllers;

use app\components\interfaces\Access;
use app\helpers\PhotoHelper;
use app\helpers\PriceHelper;
use Yii;
use app\models\{Administrations, Agency, Order, Matrix, Constant, OrderCreate, OrderImage, search\OrderSearch, User};
use yii\base\DynamicModel;
use yii\filters\{VerbFilter, AccessControl};
use yii\web\{ForbiddenHttpException, Controller, HttpException, NotFoundHttpException, Response, UploadedFile};
use function GuzzleHttp\Psr7\mimetype_from_extension;
date_default_timezone_set('Europe/Moscow');

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller implements Access
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN, Constant::ROLE_ADMINISTRATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'download-photos', 'download-all-photos'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['comment', 'send-to-agency'],
                        'roles' => [Constant::ROLE_PHOTO_MODERATOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status', 'schedule', 'update-description', 'calculate-photograph-price'],
                        'roles' => [Constant::ROLE_ADMIN_OF_PHOTOGRAPHER],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['schedule', 'fail'],
                        'roles' => [Constant::ROLE_PHOTOGRAPHER],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['reject', 'upload-photos'],
                        'roles' => [Constant::ROLE_PHOTOGRAPHER, Constant::ROLE_PHOTO_MODERATOR],
                        'matchCallback' => function ($rule, $action) {
                            $model = $this->findModel(Yii::$app->request->get('id'));
                            return in_array($model->status, [Constant::ORDER_STATUS_EXECUTING, Constant::ORDER_STATUS_MODERATION]);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['back-to-work', 'create', 'calculate-agency-price'],
                        'roles' => [Constant::ROLE_REAL_ESTATE_AGENCY],
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => [Constant::ROLE_REAL_ESTATE_AGENCY],
                        'matchCallback' => function ($rule, $action) {
                            $model = $this->findModel(Yii::$app->request->get('id'));
                            return ($model->agency_id == Yii::$app->user->identity->agency->id) && ($model->status == Constant::ORDER_STATUS_NEW);
                        }
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->checkAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @param null $ready
     * @return string
     */
    public function actionIndex($ready = null)
    {
        $this->layout = 'main_without_container';
        $searchModel = new OrderSearch();
        $role = Yii::$app->user->identity->getRole();

        if (in_array(Yii::$app->user->identity->getRole(), [Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, Constant::ROLE_REAL_ESTATE_AGENCY]) &&
            $ready !== null) {
            if ($ready == 0) {
                $searchModel->status = Constant::notReadyOrderStatuses();
            } else {
                $searchModel->status = Constant::readyOrderStatuses();
            }
        }

        if (($role == Constant::ROLE_PHOTO_MODERATOR) && $ready !== null) {
            if ($ready == 0) {
                $searchModel->status = [Constant::ORDER_STATUS_EXECUTING, Constant::ORDER_STATUS_MODERATION];
            } else {
                $searchModel->status = Constant::ORDER_STATUS_DOWNLOADED;
            }
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ready);

        return $this->render($this->getViewFile($this->action->id), [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Расписание
     * @return string
     */
    public function actionSchedule()
    {
        $identity = Yii::$app->user->identity;

        $query = (new OrderSearch())->searchSchedule();

        ($identity->getRole() == Constant::ROLE_PHOTOGRAPHER) && $query->andWhere(['photographer_id' => $identity->photographer->id]);
        $modelsByPhotographer = [];

        foreach ($query->all() as $model) {
            $modelsByPhotographer[$model->photographer_id][] = $model;
        }

        return $this->render('schedule', compact('modelsByPhotographer'));
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMIN || $role == Constant::ROLE_ADMINISTRATION) {
            $model = new OrderCreate();
        } else {
            $model = new Order();
            $model->scenario = OrderCreate::SCENARIO_CREATE_UPDATE;
        }
        ($role == Constant::ROLE_REAL_ESTATE_AGENCY) && $model->agency_id = Yii::$app->user->identity->agency->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!empty($model->zone) && !empty($model->room_qnt_type)) {
                if ($role == Constant::ROLE_REAL_ESTATE_AGENCY) {
                    $model->price_agency = (new PriceHelper())->calculatePrice('agency_id', $model);
                }
                if ($model->photographer_id != null) {
                    $model->price = (new PriceHelper())->calculatePrice('photographer_id', $model);
                }
            }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Order::SCENARIO_CREATE_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!empty($model->zone) && !empty($model->room_qnt_type)) {
                if ($model->photographer_id != null) {
                    $model->price = (new PriceHelper())->calculatePrice('photographer_id', $model);
                }
            }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can(Constant::ROLE_ADMIN) && $model->isReady()) {
            throw new HttpException(400, 'Нельзя изменить статус готовой заявки');
        }
        $model->scenario = Order::SCENARIO_CHANGE_STATUS;

        //===========ДЛЯ КАРТЫ===============
        $orderModels = (new OrderSearch())->searchOrdersMap();
        $orders = [];

        $colorQnt = count(Constant::YMAP_ICON_PRESETS);
        foreach ($orderModels as $orderModel) {
            if (!isset($orders[$orderModel->photographer_id]['color'])) {
                $orders[$orderModel->photographer_id]['color'] = Constant::YMAP_ICON_PRESETS[count($orders) % $colorQnt];
            }
            $orders[$orderModel->photographer_id]['models'][] = $orderModel;

        }
        //=====================================

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->status == 'status-failed') {
                $model->price_agency = $model->price_agency / 2;
            }

            if ($model->status == 'status-rejected') {
                $model->price_agency = 0;
                $model->price = 0;
            }

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('forms/_form_status', compact('model', 'orders'));
    }

    /**
     * @return bool|mixed|null
     * @throws NotFoundHttpException
     */
    public function actionCalculatePhotographPrice()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');

            $model = $this->findModel($id);
            $model->photographer_id = Yii::$app->request->post('photographer_id');

            if (!empty($model->zone) && !empty($model->room_qnt_type)) {
                return $price_photographer = (new PriceHelper())->calculatePrice('photographer_id', $model);
            }

            return false;
        }
    }


    /**
     * @return bool|mixed|null
     */
    public function actionCalculateAgencyPrice()
    {
        if (Yii::$app->request->isAjax) {
            $zone = Yii::$app->request->post('zone');
            $room_qnt_type = Yii::$app->request->post('room_qnt_type');

            $agency_id = Yii::$app->user->identity->agency->id;
            $model = new DynamicModel(compact('zone', 'agency_id', 'room_qnt_type'));
            $model->agency_id = $agency_id;

            if ($model->addRule(['zone', 'agency_id', 'room_qnt_type'], 'required')->validate()) {
                if (!empty($zone) && !empty($room_qnt_type)) {
                    return $price_agency = (new PriceHelper())->calculatePrice('agency_id', $model);
                }
            }

            return false;
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionBackToWork($id)
    {
        $model = $this->findModel($id);
        if ($model->status !== Constant::ORDER_STATUS_REJECTED) {
            throw new HttpException(400, 'Заявка не в статусе ' . Constant::ORDER_STATUS_REJECTED);
        }

        $model->status = Constant::ORDER_STATUS_NEW;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdateDescription($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Примечание изменено');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'success';
        }

        return $this->renderAjax('forms/_form_admin_of_photographer', compact('model'));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUploadPhotos($id)
    {
        $model = $model = $this->findModel($id);
        $model->scenario = Order::SCENARIO_UPLOAD;
        $model->status = Constant::ORDER_STATUS_MODERATION;
        $photoForm = new DynamicModel(['images']);
        $photoForm->addRule(['images'], 'file', ['skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 200, 'maxSize' => 1024 * 1024 * 1024]);

        if ($model->load(Yii::$app->request->post())) {
            $photoForm->images = UploadedFile::getInstances($photoForm, 'images');

            if ($photoForm->images && $photoForm->validate() && $model->save()) {
                $model->uploadPhotos($photoForm->images);
                return $this->redirect(['order/view', 'id' => $model->id]);
            }
        }

        return $this->render('forms/_form_upload', compact('model', 'photoForm'));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionFail($id)
    {
        $model = $model = $this->findModel($id);
        $model->scenario = Order::SCENARIO_UPLOAD;
        $model->status = Constant::ORDER_STATUS_FAILED;

        $newModel = clone $model;
        $newModel->number .= '_1';

        $model->price_agency = $model->price_agency / 2;

        $photoForm = new DynamicModel(['images']);
        $photoForm->addRule(['images'], 'file', ['skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg', 'mp3', 'aac', 'wav'], 'maxFiles' => 20, 'maxSize' => 1024 * 1024 * 1024, 'checkExtensionByMimeType' => false]);

        if ($model->load(Yii::$app->request->post())) {
            $photoForm->images = UploadedFile::getInstances($photoForm, 'images');

            if ($photoForm->validate() && $model->save() && $newModel->save()) {
                $photoForm->images && $model->uploadPhotos($photoForm->images);
                return $this->redirect(['order/view', 'id' => $model->id]);
            }
        }

        return $this->render('forms/_form_fail', compact('model', 'photoForm'));
    }

    /**
     * @param $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionDownloadPhotos($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY &&
            Yii::$app->user->identity->agency->id != $model->agency_id) {
            throw new ForbiddenHttpException('Это не Ваша заявка');
        }

        if (!empty($images = Yii::$app->request->post('images'))) {
            $images = array_keys($images);
            if (Yii::$app->request->post('delete')) {
                $role = Yii::$app->user->identity->getRole();
                if (!in_array($role, [Constant::ROLE_ADMIN, Constant::ROLE_PHOTO_MODERATOR, Constant::ROLE_PHOTOGRAPHER])) {
                    throw new ForbiddenHttpException('Вам запрещено удалять фотографии');
                }

                if ($role == Constant::ROLE_PHOTOGRAPHER && $model->status != Constant::ORDER_STATUS_MODERATION) {
                    throw new ForbiddenHttpException('Фотограф может удалять фотографии только в статусе "На модерации"');
                }

                PhotoHelper::deletePhotos($images);
            } else {
                PhotoHelper::downloadPhotos($model, $images);
            }

            return $this->redirect(['order/view', 'id' => $id]);

        } else {
            throw new HttpException(400, 'Фотографии отсутствуют');
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDownloadAllPhotos($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY &&
            Yii::$app->user->identity->agency->id != $model->agency_id) {
            throw new ForbiddenHttpException('Это не Ваша заявка');
        }

        $images = [];
        foreach ($model->images as $image) {
            $images[] = $image->image;
        }

        PhotoHelper::downloadPhotos($model, $images);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionReject($id)
    {
        $model = $this->findModel($id);
        $model->status = Constant::ORDER_STATUS_REJECTED;
        $model->save();

        return $this->redirect(['order/view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws HttpException
     */
    public function actionSendToAgency($id)
    {
        $model = $this->findModel($id);
        if (empty($model->images)) {
            throw new HttpException(400, 'Нельзя отправить АН заявку без фото');
        }
        $model->status = Constant::ORDER_STATUS_DOWNLOADED;
        $model->save();

        return $this->redirect(['order/view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionComment($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('forms/_form_comment', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        foreach ($model->images as $image) {
            $image->delete();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $actionId
     * @return string
     */
    protected function getViewFile($actionId)
    {
        return $actionId . '/' . Yii::$app->user->identity->getRole();
    }

    /**
     * @param $id
     *
     * @return bool
     *
     * @throws NotFoundHttpException
     */
    public function checkAccess($id)
    {
        $role = Yii::$app->user->identity->getRole();

        if ($role == Constant::ROLE_ADMIN) {
            return true;
        }

        $model = $this->findModel(Yii::$app->request->get('id'));
        $identity = Yii::$app->user->identity;

        if ($role == Constant::ROLE_ADMINISTRATION) {
            $agency = Agency::findOne(['id' => $model->agency_id]);

            return ($identity->administration->id == $agency->administration_id);
        }

        if ($role == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER || $role == Constant::ROLE_PHOTO_MODERATOR) {
            $agency = Agency::findOne(['id' => $model->agency_id]);
            $adminPhotographer = User::findOne(['id' => $identity->user->id]);

            return ($adminPhotographer->administration_id == $agency->administration_id);
        }

        if ($role == Constant::ROLE_REAL_ESTATE_AGENCY) {
            return ($identity->agency->id == $model->agency_id);
        }

        if ($role == Constant::ROLE_PHOTOGRAPHER) {
            return ($identity->photographer->id == $model->photographer_id);
        }

        return false;
    }
}
