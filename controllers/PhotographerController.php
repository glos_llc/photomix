<?php

namespace app\controllers;


use app\components\interfaces\Access;
use Yii;
use app\models\{Administrations, Agency, Constant, Identity, Photographer, search\PhotographerSearch, User};
use yii\filters\{AccessControl, VerbFilter};
use yii\web\{HttpException, NotFoundHttpException, Controller};

/**
 * PhotographerController implements the CRUD actions for Photographer model.
 */
class PhotographerController extends Controller implements Access
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create'],
                        'roles' => [Constant::ROLE_ADMIN],
                    ],
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN, Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, Constant::ROLE_ADMINISTRATION],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Photographer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhotographerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photographer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionView($id)
    {
        $this->checkAccess($id);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photographer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $identity = new Identity();
        $model = new Photographer(['identity' => $identity->id]);

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $identity->load(Yii::$app->request->post()) &&
            $identity->save() && $model->save()) {

            Yii::$app->authManager->assign(Yii::$app->authManager->getRole(Constant::ROLE_PHOTOGRAPHER), $identity->id);
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', compact('model', 'identity'));
    }

    /**
     * Updates an existing Photographer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $this->checkAccess($id);

        $model = $this->findModel($id);
        $identity = $model->entity;

        if ($model->load(Yii::$app->request->post()) && $identity->load(Yii::$app->request->post()) &&
            $identity->save() && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', compact('model', 'identity'));
    }

    /**
     * Deletes an existing Photographer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->checkAccess($id);

        $model = $this->findModel($id);
        Yii::$app->authManager->revokeAll($model->entity->id);
        $model->entity->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photographer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photographer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photographer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @throws HttpException
     */
    public function checkAccess($id)
    {
        $role = Yii::$app->user->identity->getRole();

        switch ($role) {
            case Constant::ROLE_ADMINISTRATION:
                if (Photographer::findOne([$id])->administration_id != Administrations::findOne(['identity' => Yii::$app->user->id])->id) {
                    throw new HttpException(403, 'Forbidden');
                }
                break;
            case $role == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER:
                if ($this->findModel($id)->administration_id != User::findOne(['identity' => Yii::$app->user->id])->id) {
                    throw new HttpException(403, 'Forbidden');
                }
                break;
        }
    }
}
