<?php

namespace app\controllers;


use app\components\interfaces\Access;
use app\models\{Administrations, search\AgencySearch, Agency, Identity, Constant};
use yii\web\{HttpException, NotFoundHttpException, Controller};
use yii\filters\{AccessControl, VerbFilter};
use Yii;


/**
 * AgencyController implements the CRUD actions for Agency model.
 */
class AgencyController extends Controller implements Access
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create'],
                        'roles' => [Constant::ROLE_ADMIN],
                    ],
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN, Constant::ROLE_ADMINISTRATION],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Agency models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agency model.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionView($id)
    {
        $this->checkAccess($id);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     *
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $identity = new Identity();
        $model = new Agency(['identity' => $identity->id]);

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $identity->load(Yii::$app->request->post()) &&
            $identity->save() && $model->save()) {

            Yii::$app->authManager->assign(Yii::$app->authManager->getRole(Constant::ROLE_REAL_ESTATE_AGENCY), $identity->id);
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', compact('model', 'identity'));
    }

    /**
     * Updates an existing Agency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $this->checkAccess($id);

        $model = $this->findModel($id);
        $identity = $model->entity;

        if ($model->load(Yii::$app->request->post()) && $model->entity->load(Yii::$app->request->post()) &&
            $model->entity->save() && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', compact('model', 'identity'));
    }

    /**
     * Deletes an existing Agency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->checkAccess($id);

        $model = $this->findModel($id);
        Yii::$app->authManager->revokeAll($model->entity->id);
        $model->entity->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Agency the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agency::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function checkAccess($id)
    {
        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            if ($this->findModel($id)->administration_id != Administrations::findOne(['identity' => Yii::$app->user->id])->id) {
                throw new HttpException(403, 'Forbidden');
            }
        }
    }
}
