<?php


namespace app\controllers;


use app\models\Constant;
use app\models\Order;
use app\models\search\OrderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class MapController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['agency'],
                        'roles' => [Constant::ROLE_REAL_ESTATE_AGENCY, Constant::ROLE_ADMINISTRATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['orders', 'get-json-object-for-map'],
                        'roles' => [Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, Constant::ROLE_ADMINISTRATION],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionAgency()
    {
        return $this->render('agency');
    }

    public function actionOrders()
    {
        return $this->render('orders');
    }

    /**
     * @return array|null
     */
    private function getFilteredOrders(): ?array
    {
        $query = (new OrderSearch())->getQuery();
        //todo added filters by role
        $orderModels = $query
            ->andWhere(['status' => [
                Constant::ORDER_STATUS_NEW,
                Constant::ORDER_STATUS_PRELIM_CALL,
                Constant::ORDER_STATUS_MISSED_CALL_1,
                Constant::ORDER_STATUS_MISSED_CALL_2,
            ]])
            ->orderBy('created_at DESC')->all();

        $orders = [];
        foreach ($orderModels as $model) {
            $orders[$model->status][] = $model;
        }

        return $orders;
    }

    public function actionGetJsonObjectForMap()
    {
        if (Yii::$app->request->isAjax) {
            $orders = $this->getFilteredOrders();

            $jsonObject = [
                "type" => "FeatureCollection",
                "features" => [],
            ];

            $z = 0;
            $isNsk = true;
            foreach ($orders as $orderStatus => $orderArray) {
                $iconClass = '';
                switch ($orderStatus) {
                    case Constant::ORDER_STATUS_MISSED_CALL_1:
                    case Constant::ORDER_STATUS_MISSED_CALL_2:
                        $iconClass = 'islands#yellowStretchyIcon';
                        break;
                    case Constant::ORDER_STATUS_PRELIM_CALL:
                        $iconClass = 'islands#violetStretchyIcon';
                        break;
                    default:
                        $iconClass = 'islands#blueStretchyIcon';
                }
                for ($i = 0, $length = count($orderArray); $i < $length; $i++) {
                    $planningContactDate = $orderArray[$i]->planing_contact_date ? date('d.m', strtotime($orderArray[$i]->planing_contact_date)) : '';
                    $assignLink = "<a href='/order/change-status?id=" . $orderArray[$i]->id . "'>Назначить фотографа</a>";
                    $isNsk = $isNsk && $orderArray[$i]->isInNsk();
                    $coordinates = explode(", ", $orderArray[$i]->getCoordinatesForMap());
                    array_push($jsonObject["features"], [
                        "type" => "Feature",
                        "id" => $z,
                        "geometry" => [
                            "type" => "Point",
                            "coordinates" =>
                                [(double)$coordinates[0], (double)$coordinates[1]],
                        ],
                        "properties" => [
                            "balloonContentHeader" => "<a href='/order/view?id=" . $orderArray[$i]->id . "'>Заявка №" . $orderArray[$i]->number . "</a>",
                            "balloonContentBody" => "<a href='/order/view?id=" . $orderArray[$i]->id . "'>Заявка №" . $orderArray[$i]->number . "</a>",
                            "balloonContentFooter" => "<p>" . $orderArray[$i]->address . "<br>" . $orderArray[$i]->shortPeriod . "<br>Статус: " . $orderArray[$i]->getStatus() . "<br>Дата созвона: " . $planningContactDate . "<br> " . $assignLink . " <br></p>",
                            "clusterCaption" => "Заявка №'" . $orderArray[$i]->number,
                        ],
                        "options" => [
                            "preset" => $iconClass,
                        ],
                    ]);
                    $z++;
                }
            }

            return json_encode($jsonObject, JSON_HEX_TAG);
        }
    }
}