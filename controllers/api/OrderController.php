<?php

namespace app\controllers\api;

use app\models\{Order,Constant};
use Yii;
use yii\filters\AccessControl;
use yii\web\{Response,Controller,NotFoundHttpException};
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'change-price' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constant::ROLE_ADMIN, Constant::ROLE_ADMINISTRATION],
                    ],
                ],
            ],
            [
                'class' => 'yii\filters\AjaxFilter',
            ],
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
        ];
    }

    /**
     * Изменение цены в индекс странице заказов
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangePrice()
    {
        $price = Yii::$app->request->post('value', null);
        $agency = (bool)Yii::$app->request->post('agency', false);
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($price !== null) {
            $model->{$agency ? 'price_agency' : 'price'} = abs((int)$price);

            return $model->save();
        }

        return false;
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
