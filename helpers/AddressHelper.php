<?php


namespace app\helpers;


class AddressHelper
{
    public function getAddress($coordinates)
    {
        $options = array(
            "http" => array(
                "header" => "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
            )
        );
        $coordinates = explode(', ', $coordinates);

        $context = stream_context_create($options);

        return file_get_contents('https://geocode-maps.yandex.ru/1.x/?apikey=30071976-3b4b-46b5-9531-5d5594e49e96&geocode=' . $coordinates[1] . ',' . $coordinates[0], false, $context);
    }

    public function parseAddress($yandexXml, &$city, &$street, &$house)
    {
        $yandexXml = simplexml_load_string($yandexXml);
        if(isset($yandexXml->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->Address)){
            $data = $yandexXml->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->Address;
            foreach ($data->Component as $item) {
                switch ($item->kind) {
                    case 'locality':
                        $city = $item->name->__toString();
                        break;
                    case 'street':
                        $street = $item->name->__toString();
                        break;
                    case 'house':
                        $house = $item->name->__toString();
                        break;
                }
            }
            if (!$street && $city && $house) {
                $address = explode(', ', $data->formatted);
                $street = $address[count($address) - 2];
            }
            if (!$street || !$city || !$house) {
                return false;
            }
            return true;
        }
        return false;
    }
}