<?php


namespace app\helpers;


use app\models\Constant;
use app\models\Matrix;

class PriceHelper
{
    /**
     * @param $attribute
     * @param $model
     *
     * @return mixed|null
     */
    public function calculatePrice($attribute, $model)
    {
        $matrix = Matrix::findOne([$attribute => $model->$attribute]);

        if ($matrix == null) {
            return null;
        }

        $array_room = Constant::qntRoomTypes();
        for ($i = 0, $length = count($array_room); $i < $length; $i++) {
            if (is_numeric(strpos($model->room_qnt_type, $array_room[$i]))) {
                $stringRoom = $this->getStringRoom($array_room[$i], $array_room);
                break;
            }
        }

        $array_zone = Constant::getZoneTypes();
        for ($i = 0, $length = count($array_zone); $i < $length; $i++) {
            if (is_numeric(strpos($model->zone, $array_zone[$i]))) {
                $stringZone = $this->getStringZone($array_zone[$i], $array_zone);
                break;
            }
        }
        $attribute = $stringRoom . '_' . $stringZone;
        return $matrix->$attribute;
    }

    /**
     * @param $i
     * @param $array_room
     *
     * @return string
     */
    private function getStringRoom($i, $array_room)
    {
        $room = '';
        switch ($i) {
            case $array_room[0]:
                $room = 'one_room';
                break;
            case $array_room[1]:
                $room = 'two_room';
                break;
            case $array_room[2]:
                $room = 'three_room';
                break;
            case $array_room[3]:
                $room = 'four_room';
                break;
            case $array_room[4]:
                $room = 'house';
                break;
        }

        return $room;
    }

    /**
     * @param $i
     * @param $array_zone
     *
     * @return string
     */
    private function getStringZone($i, $array_zone)
    {
        $zone = '';
        switch ($i) {
            case $array_zone[0]:
                $zone = 'first_zone';
                break;
            case $array_zone[1]:
                $zone = 'second_zone';
                break;
            case $array_zone[2]:
                $zone = 'third_zone';
                break;
            case $array_zone[3]:
                $zone = 'fourth_zone';
                break;
            case $array_zone[4]:
                $zone = 'fifth_zone';
                break;
        }

        return $zone;
    }
}