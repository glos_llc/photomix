<?php

namespace app\helpers;

use yii\db\Exception;
use yii\web\HttpException;

abstract class GeoHelper
{
    const API_URL = 'https://geocode-maps.yandex.ru/1.x/';
    const API_KEY = '30071976-3b4b-46b5-9531-5d5594e49e96';


    /**
     * @param string $city
     * @param string $street
     * @param string $house
     * @return array|null
     * @throws HttpException
     */
    static function getCoordinates(string $city, string $street, string $house): ?string
    {
        try {
            $response = json_decode(self::request($city, $street, $house), true);
            if (self::checkResponse($response)) {
                $coordinates = $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];

                return $coordinates ? implode(' ', array_reverse(explode(' ', $coordinates))) : null;
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'geocode-api');
        }

        return null;
    }

    /**
     * @param $response
     * @return bool
     */
    private static function checkResponse($response)
    {
        if (json_last_error() === JSON_ERROR_NONE && is_array($response)) {
            if ((int)$response['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'] === 1) {
                $addressElements = $response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'];

                foreach ($addressElements as $element) {
                    if ($element['kind'] == 'house') {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param string $city
     * @param string $street
     * @param string $house
     * @return string
     */
    private static function request(string $city, string $street, string $house): string
    {
        $options = array(
            "http" => array(
                "header" => "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
            )
        );

        $context = stream_context_create($options);
        $response = file_get_contents(self::getRequestUrl($city, $street, $house), false, $context);

        if ($response === false) {
            throw new HttpException(500, 'Сервис првоерки адреса недоступен');
        }

        return $response;
    }

    /**
     * @param string $city
     * @param string $street
     * @param string $house
     * @return string
     */
    private static function getRequestUrl(string $city, string $street, string $house)
    {
        $address = str_replace(' ', '+', "$city улица $street дом $house");

        $query_array = array(
            'apikey' => self::API_KEY,
            'format' => 'json',
            'geocode' => $address,
            'results' => 1,
        );

        $query = http_build_query($query_array);
        return self::API_URL . '?' . $query;
    }
}