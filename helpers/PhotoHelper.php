<?php


namespace app\helpers;


use app\models\Constant;
use app\models\Order;
use app\models\OrderImage;
use Yii;

class PhotoHelper
{
    /**
     * @param Order $model
     * @param array $images
     */
    public static function downloadPhotos(Order $model, array $images)
    {
        $address = $model->address;
        if (strlen($address) > 100) {
            $address = mb_substr($model->street . ', ' . $model->house . ', кв. ' . $model->apartment_number, 0, 100);
        }
        $address = str_replace(['\\', '/'], '-', $address);
        $address = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $address);

        $zipFileName = Yii::getAlias('@webroot') . '/uploads/' . date('Y_m_d') . '_' . $address . '.zip';

        $zip = new \ZipArchive();

        $zip->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $counter = 0;
        foreach ($images as $image) {
            $fullPath = Yii::getAlias('@webroot') . '/' . OrderImage::IMAGE_PATH . $image;
            if (file_exists($fullPath)) {
                $zip->addFile($fullPath, ++$counter . '_' . $image);
            }
        }

        $zip->close();

        Yii::$app->response->sendFile($zipFileName)->on(\yii\web\Response::EVENT_AFTER_SEND, function ($event) {
            unlink($event->data);
        }, $zipFileName);

        if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY) {
            $model->downloaded = true;
            $model->save();
        }

        Yii::$app->response->send();
    }

    /**
     * @param array $images
     * @return \yii\web\Response
     */
    public static function deletePhotos(array $images)
    {
        if (!empty($images)) {
            OrderImage::deleteAll(['image' => $images]);
            foreach ($images as $image) {
                @unlink(OrderImage::IMAGE_PATH . $image);
            }
        }
    }
}