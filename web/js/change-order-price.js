"use strict";

$(document).ready(function() {
    let protocol = location.protocol;
    let host = '//'.concat(window.location.hostname);

    $('.change-price-input').on('change', function (e) {
        let input = $(this);
        $.ajax({
            type: "POST",
            showNoty: false,
            url:  host + '/api/order/change-price',
            data: {'id':input.data('id'), 'value':input.val(), 'agency':input.data('agency')},
            success: function (res) {
                console.log(res);
            },
            error: function(){
                alert('Error!');
            }
        });
    });
});