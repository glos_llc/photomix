<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $identity
 * @property string $name
 * @property string $status
 *
 * @property Identity $entity
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 * @property int $administration_id [int(11)]
 *
 * @property Administrations $administrator
 */
class User extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    public $role;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['identity'], 'string', 'max' => 36],
            [['name'], 'string', 'max' => 255],
            ['administration_id', 'integer'],
            [['role'], 'in', 'range' => [Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, Constant::ROLE_PHOTO_MODERATOR]],
            [['identity'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::className(), 'targetAttribute' => ['identity' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'identity' => 'Логин',
            'name' => 'ФИО',
            'role' => 'Роль',
            'created' => 'Создан',
            'updated' => 'Обновлен',
            'administration_id' => 'Администратор',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Identity::className(), ['id' => 'identity']);
    }

    /**
     * @return null|string
     */
    public function getRole() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->entity->id);
        if (is_array($role)) {
            return key($role);
        }
        return null;
    }

    /**
     * @return null|string
     */
    public function getRoleDescription() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->entity->id);
        if (is_array($role)) {
            return Constant::roles(true)[key($role)];
        }
        return null;
    }

    public function block()
    {
        $this->entity->status = Identity::STATUS_BLOCKED;
        return $this->entity->save();
    }

    public function activate()
    {
        $this->entity->status = Identity::STATUS_ACTIVE;
        return $this->entity->save();
    }

    public function beforeSave($insert)
    {
        $this->administration_id = Administrations::findOne(['identity' => Yii::$app->user->id])->id;
        return parent::beforeSave($insert);
    }

    public function getAdministrator()
    {
        return $this->hasOne(Administrations::class, ['id' => 'administration_id']);
    }
}
