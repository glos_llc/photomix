<?php

namespace app\models\labels;

class OrderLabel
{
    /**
     * @param $attr
     * @return string
     */
    public static function getAttributeLabel($attr): string
    {
        return self::getAttributeLabels()[$attr] ?? '';
    }

    /**
     * @return array
     */
    public static function getAttributeLabels(): array
    {
        return [
            'rrId' => 'Id RealtyRuler',
            'id' => 'ID',
            'number' => '№',
            'coloredNumber' => '№',
            'agency_id' => 'АН',
            'realtor' => 'Риелтор',
            'status' => 'Статус',
            'room_qnt_type' => 'Комнатность',
            'address' => 'Адрес',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'description' => 'Прим. АН',
            'coordinates' => 'Координты',
            'description_admin' => 'Прим.админ.',
            'client_name' => 'Клиент',
            'phone_number' => 'Телефон клиента',
            'execution_date_from' => 'Дата исполнения с ',
            'execution_date_to' => 'Дата исполнения по ',
            'planing_contact_date' => 'План. дата созвона',
            'changing_status_date' => 'Дата изменения статуса',
            'price' => 'Цена для фотографа',
            'price_agency' => 'Цена для АН',
            'photographer_id' => 'Фотограф',
            'photographer_comment' => 'Комментарий фотографа',
            'downloaded' => 'Скачано? ',
            'created' => 'Дата создания',
            'updated' => 'Обновлено',
            'roomQntType' => 'Комнатность',
            'apartment_number' => 'Квартира',
            'period' => 'Дата оисполнения',
            'call_history' => 'Истрия звонка',
            'zone' => 'Зона',
        ];
    }
}