<?php

namespace app\models\search;

use app\models\Administrations;
use app\models\Constant;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Matrix;

/**
 * MatrixSearch represents the model behind the search form of `app\models\Matrix`.
 */
class MatrixSearch extends Matrix
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'photographer_id', 'agency_id', 'one_room_first_zone', 'two_room_first_zone', 'three_room_first_zone',
                'four_room_first_zone', 'house_first_zone', 'one_room_second_zone', 'two_room_second_zone',
                'three_room_second_zone', 'four_room_second_zone', 'house_second_zone', 'one_room_third_zone',
                'two_room_third_zone', 'three_room_third_zone', 'four_room_third_zone', 'house_third_zone',
                'one_room_fourth_zone', 'two_room_fourth_zone', 'three_room_fourth_zone', 'four_room_fourth_zone',
                'house_fourth_zone', 'one_room_fifth_zone', 'two_room_fifth_zone', 'three_room_fifth_zone',
                'four_room_fifth_zone', 'house_fifth_zone', 'created_at', 'updated_at'], 'integer'],
            ['administration', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Matrix::find();

        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            $query->joinWith('agency')->joinWith('photograph');
            $query->where(['real_estate_agency.administration_id' =>
                Administrations::findOne(['identity' => Yii::$app->user->id])->id])
                ->orWhere(['photographer.administration_id' => Administrations::findOne(['identity' => Yii::$app->user->id])->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'photographer_id' => $this->photographer_id,
            'agency_id' => $this->agency_id,
            'one_room_first_zone' => $this->one_room_first_zone,
            'two_room_first_zone' => $this->two_room_first_zone,
            'three_room_first_zone' => $this->three_room_first_zone,
            'four_room_first_zone' => $this->four_room_first_zone,
            'house_first_zone' => $this->house_first_zone,
            'one_room_second_zone' => $this->one_room_second_zone,
            'two_room_second_zone' => $this->two_room_second_zone,
            'three_room_second_zone' => $this->three_room_second_zone,
            'four_room_second_zone' => $this->four_room_second_zone,
            'house_second_zone' => $this->house_second_zone,
            'one_room_third_zone' => $this->one_room_third_zone,
            'two_room_third_zone' => $this->two_room_third_zone,
            'three_room_third_zone' => $this->three_room_third_zone,
            'four_room_third_zone' => $this->four_room_third_zone,
            'house_third_zone' => $this->house_third_zone,
            'one_room_fourth_zone' => $this->one_room_fourth_zone,
            'two_room_fourth_zone' => $this->two_room_fourth_zone,
            'three_room_fourth_zone' => $this->three_room_fourth_zone,
            'four_room_fourth_zone' => $this->four_room_fourth_zone,
            'house_fourth_zone' => $this->house_fourth_zone,
            'one_room_fifth_zone' => $this->one_room_fifth_zone,
            'two_room_fifth_zone' => $this->two_room_fifth_zone,
            'three_room_fifth_zone' => $this->three_room_fifth_zone,
            'four_room_fifth_zone' => $this->four_room_fifth_zone,
            'house_fifth_zone' => $this->house_fifth_zone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
