<?php

namespace app\models\search;

use app\models\Administrations;
use app\models\Constant;
use app\models\Identity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    public $login;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'identity'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $role = Yii::$app->user->identity->getRole();
        if ($role != Constant::ROLE_ADMINISTRATION) {
            if ($this->identity) {
                $query->innerJoinWith('entity');
                $query->andWhere('like', Identity::tableName() . '.login', $this->login);
            }
        }else{
            $query->where(['user.administration_id' => Administrations::findOne(['identity' => Yii::$app->user->id])->id]);
            if ($this->identity) {
                $query->andWhere('like', Identity::tableName() . '.login', $this->login);
            }
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if ($this->identity) {
            $query->innerJoinWith('entity');
            $query->andWhere(['like', Identity::tableName() . '.login', $this->identity]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);





        return $dataProvider;
    }
}
