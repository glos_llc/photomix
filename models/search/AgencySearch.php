<?php

namespace app\models\search;

use app\models\Administrations;
use app\models\Constant;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Agency;

/**
 * AgencySearch represents the model behind the search form of `app\models\Agency`.
 */
class AgencySearch extends Agency
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['identity', 'name', 'city'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agency::find()->joinWith('administrations');

        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            $query->where(['real_estate_agency.administration_id' => Administrations::findOne(['identity' => Yii::$app->user->id])->id]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $dataProvider->sort->attributes['city'] = [
            'asc' => ['city' => SORT_ASC],
            'desc' => ['city' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'identity', $this->identity])
            ->andFilterWhere(['like', 'real_estate_agency.name', $this->name])
            ->andFilterWhere(['like', 'administrations.city', $this->city])
        ;

        return $dataProvider;
    }
}
