<?php

namespace app\models\search;

use app\models\Administrations;
use app\models\Agency;
use app\models\Constant;
use app\models\Photographer;
use app\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    public $address;
    public $date_from;
    public $date_to;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        if (Yii::$app->user->can(Constant::ROLE_ADMIN)|| Yii::$app->user->can(Constant::ROLE_ADMINISTRATION)) {
            return [
                [['id', 'price'], 'integer'],
                [['realtor', 'status', 'room_qnt_type', 'address', 'description', 'photographer_id', 'number',
                    'client_name', 'phone_number', 'agency_id', 'date_from', 'date_to'], 'string'],
            ];
        } elseif (Yii::$app->user->can(Constant::ROLE_ADMIN_OF_PHOTOGRAPHER)) {
            return [
                [['price'], 'integer'],
                [['realtor', 'room_qnt_type', 'address', 'description', 'photographer_id', 'number', 'status',
                    'client_name', 'phone_number', 'agency_id', 'date_from', 'date_to'], 'string'],
            ];
        } elseif (Yii::$app->user->can(Constant::ROLE_PHOTOGRAPHER)) {
            return [
                [['address', 'realtor', 'date_from', 'date_to', 'status'], 'string']
            ];
        }

        return [
            [['address', 'realtor', 'date_from', 'date_to', 'number'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'date_from' => 'Дата исполнения с ',
            'date_to' => 'Дата исполнения по ',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $ready
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ready = null)
    {
        $query = $this->getQuery();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]]
        ]);

        $this->load($params);
        $status = $this->status;

        $role = Yii::$app->user->identity->getRole();
        switch ($role) {
            case Constant::ROLE_ADMIN_OF_PHOTOGRAPHER:
                !$status && $status = $ready ? Constant::readyOrderStatuses() : Constant::notReadyOrderStatuses();
                is_array($status) && $this->status = null;
                break;
            case Constant::ROLE_REAL_ESTATE_AGENCY:
                $this->agency_id = Yii::$app->user->identity->agency->id;
                break;
            case Constant::ROLE_PHOTOGRAPHER:
                $this->photographer_id = Yii::$app->user->identity->photographer->id;
                !$status && $status = [Constant::ORDER_STATUS_EXECUTING, Constant::ORDER_STATUS_DOWNLOADED, Constant::ORDER_STATUS_MODERATION, Constant::ORDER_STATUS_FAILED];
                break;
            case Constant::ROLE_PHOTO_MODERATOR:
                !$status && $status = [Constant::ORDER_STATUS_EXECUTING, Constant::ORDER_STATUS_DOWNLOADED, Constant::ORDER_STATUS_MODERATION];
                break;

        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->photographer_id === 'null') {
            $query->andWhere(['photographer_id' => null]);
        } else {
            $query->andFilterWhere(['photographer_id' => $this->photographer_id]);
        }

        //Фильтр по дате
        if ($this->date_from && $this->date_to) {
            $query->andWhere([
                'and',
                ['>=', 'execution_date_from', date('Y-m-d H:i:s', strtotime($this->date_from))],
                ['<=', 'execution_date_to', date('Y-m-d H:i:s', strtotime($this->date_to))]
            ]);
        } else {
            if ($this->date_from) {
                $date = date('Y-m-d H:i:s', strtotime($this->date_from));
                $query->andWhere([
                    'or',
                    ['>=', 'execution_date_from', $date],
                    ['>=', 'execution_date_to', $date],
                ]);
            }

            if ($this->date_to) {
                $date = date('Y-m-d H:i:s', strtotime($this->date_to));
                $query->andWhere([
                    'or',
                    ['<=', 'execution_date_from', $date],
                    ['<=', 'execution_date_to', $date],
                ]);
            }
        }

        if ($this->address) {
            $query->andFilterWhere(['<>',
                'POSITION(\'' . $this->address . '\' IN CONCAT([[city]], \' \', [[street]], \' \', [[house]], \' \', [[apartment_number]]))
                ', 0]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'agency_id' => $this->agency_id,
            'status' => $status,
            'room_qnt_type' => $this->room_qnt_type,
        ]);

        $query->andFilterWhere(['like', 'realtor', $this->realtor]);
        $query->andFilterWhere(['like', 'description', $this->description]);
        $query->andFilterWhere(['like', 'client_name', $this->client_name]);
        $query->andFilterWhere(['like', 'phone_number', $this->phone_number]);
        $query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', 'price', $this->price]);
        $query->andFilterWhere(['like', 'number', $this->number]);

        //var_dump($query->createCommand()->rawSql);die;
        return $dataProvider;
    }

    /**
     * Return filtered Query
     */
    public function getQuery()
    {
        $role = Yii::$app->user->identity->getRole();
        $query = Order::find();

        if ($role == Constant::ROLE_ADMINISTRATION) {
            $administrator_id = Administrations::findOne(['identity' => Yii::$app->user->id]);
            $this->getFilterQuery($query, $administrator_id, 'id');
        } else if ($role == Constant::ROLE_REAL_ESTATE_AGENCY) {
            $id_agency = Agency::findOne(['identity' => Yii::$app->user->id]);
            $this->getFilterQuery($query, $id_agency, 'administration_id');
        } else if ($role == Constant::ROLE_PHOTOGRAPHER) {
            $id_photographer = Photographer::findOne(['identity' => Yii::$app->user->id]);
            $this->getFilterQuery($query, $id_photographer, 'administration_id');
        } else if ($role == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER || $role == Constant::ROLE_PHOTO_MODERATOR) {
            $id_user = User::findOne(['identity' => Yii::$app->user->id]);
            $this->getFilterQuery($query, $id_user, 'administration_id');
        }

        return $query;
    }

    /**
     * Filter for displaying orders for by administrators
     *
     * @param $query
     * @param $filter
     * @param $attribute
     */
    private function getFilterQuery(&$query, $filter, $attribute)
    {
        $array_agency = Agency::findAll(['administration_id' => $filter->$attribute]);

        $ids_agency = ArrayHelper::getColumn($array_agency, 'id');
        $query->where(['agency_id' => $ids_agency]);
    }

    /**
     * Get orders for schedule
     *
     * @return \yii\db\ActiveQuery
     */
    public function searchSchedule()
    {
        $query = $this->getQuery();
        $role = Yii::$app->user->identity->getRole();

        if ($role == Constant::ROLE_ADMIN) {
            $query->where(['status' => Constant::ORDER_STATUS_EXECUTING])
                ->orderBy('execution_date_from ASC');
        } else {
            $query->andWhere(['status' => Constant::ORDER_STATUS_EXECUTING])
                ->orderBy('execution_date_from ASC');
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function searchOrdersMap()
    {
        $query = $this->getQuery();
        $role = Yii::$app->user->identity->getRole();

        if ($role == Constant::ROLE_ADMIN) {
            $query->with('photographer')->where(['status' => Constant::ORDER_STATUS_EXECUTING]);
        } else {
            $query->with('photographer')->andWhere(['status' => Constant::ORDER_STATUS_EXECUTING]);
        }

        return $query->orderBy('created_at DESC')->all();
    }
}
