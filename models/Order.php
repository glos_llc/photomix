<?php

namespace app\models;

use app\components\traits\{OrderValidate, CreatedUpdated};
use app\helpers\GeoHelper;
use app\models\labels\OrderLabel;
use yii\validators\DateValidator;
use yii\web\UploadedFile;
use Yii;


/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $number
 * @property int $agency_id
 * @property string|null $realtor
 * @property string $status
 * @property string $room_qnt_type
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $address
 * @property string $coordinates
 * @property string|null $description
 * @property string|null $description_admin
 * @property string|null $client_name
 * @property string|null $phone_number
 * @property string $execution_date_from
 * @property string $execution_date_to
 * @property string|null $planing_contact_date
 * @property string|null $changing_status_date
 * @property float $price
 * @property float $price_agency
 * @property int $photographer_id
 * @property string|null $photographer_comment
 * @property string $apartment_number
 * @property int|null $downloaded
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Agency $agency
 * @property Photographer $photographer
 * @property OrderImage[] $images
 * @property string $call_history История звонка
 * @property bool $zone [tinyint(3)]  Зона
 * @property int $rrId [int(11)]  id RealtyRuler
 */
class Order extends \yii\db\ActiveRecord
{
    public $checker;
    use CreatedUpdated;
    use OrderValidate;

    const SCENARIO_CHANGE_STATUS = 'change-status';
    const SCENARIO_UPDATE_BY_AGENCY = 'update-by-agency';
    const SCENARIO_ONLY_DESCRIPTION_ADMIN = 'description-admin';
    const SCENARIO_ONLY_STATUS = 'only-status';
    const SCENARIO_UPLOAD = 'upload';
    const SCENARIO_CREATE_UPDATE = 'create_update';

    public function __clone()
    {
        parent::__clone();
        $this->id = null;
        $this->status = Constant::ORDER_STATUS_NEW;
        $this->isNewRecord = true;
        $this->created_at = $this->updated_at = time();
        $this->changing_status_date = $this->planing_contact_date = null;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['checker', 'checked'],
            [['zone', 'room_qnt_type'], 'required', 'on' => self::SCENARIO_CREATE_UPDATE],
            [['agency_id', 'client_name', 'realtor', 'apartment_number'], 'required'],
            [['planing_contact_date'], 'required', 'when' => function ($model) {
                return in_array($model->status, [Constant::ORDER_STATUS_PRELIM_CALL]);
            }, 'whenClient' => "function (attribute, value) {
                    let ddl = $('#order-status');
                    return (ddl.val() == " . Constant::ORDER_STATUS_PRELIM_CALL . ");
             }"],
            [['execution_date_from', 'execution_date_to', 'photographer_id', 'price'], 'required', 'when' => function ($model) {
                return $model->status == Constant::ORDER_STATUS_EXECUTING;
            }, 'whenClient' => "function (attribute, value) {
                    let ddl = $('#order-status');
                    return ddl.val() == " . Constant::ORDER_STATUS_EXECUTING . ";
             }"],
            [['agency_id', 'photographer_id', 'created_at', 'updated_at', 'zone'], 'integer'],
            [['description', 'photographer_comment', 'description_admin', 'coordinates'], 'string'],
            [['execution_date_from', 'execution_date_to', 'planing_contact_date', 'changing_status_date'], 'date', 'format' => 'php:d.m.Y H:i'],
            [['execution_date_to'], 'compare', 'compareAttribute' => 'execution_date_from', 'operator' => '>=', 'when' => function ($model) {
                return $this->execution_date_from;
            }],
            [['price', 'price_agency'], 'number'],
            [['number'], 'unique'],
            [['number'], 'default', 'value' => function ($model) {
                $id = (int)Order::find()->select('max(id)')->column()[0] + 1;
                return Yii::$app->getUser()->identity->getRole() === Constant::ROLE_REAL_ESTATE_AGENCY ?
                    Yii::$app->getUser()->identity->agency->name . '_' . $id :
                    'admin_' . $id;
            }],
            [['downloaded'], 'boolean'],
            [['downloaded'], 'default', 'value' => false],
            [['status'], 'default', 'value' => Constant::ORDER_STATUS_NEW],
            [['room_qnt_type'], 'in', 'range' => Constant::qntRoomTypes()],
            [['zone'], 'in', 'range' => Constant::getZoneTypes()],
            [['status'], 'in', 'range' => Constant::orderStatuses()],
            [['realtor', 'client_name', 'phone_number', 'number', 'city', 'street', 'house'], 'string', 'max' => 255],
            [['status', 'room_qnt_type'], 'string', 'max' => 30],
            [['agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['agency_id' => 'id']],
            [['photographer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Photographer::className(), 'targetAttribute' => ['photographer_id' => 'id']],

            ['call_history', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return OrderLabel::getAttributeLabels();
    }

    public function scenarios()
    {
        $scenarios = [
            self::SCENARIO_CHANGE_STATUS =>
                ['status', 'photographer_id', 'execution_date_from', 'execution_date_to', 'planing_contact_date', 'price'],
            self::SCENARIO_UPDATE_BY_AGENCY => [
                'number', 'city', 'street', 'house', 'room_qnt_type', 'client_name', 'phone_number', 'realtor', 'description'
            ],
            self::SCENARIO_ONLY_STATUS => [
                'status'
            ],
            self::SCENARIO_ONLY_DESCRIPTION_ADMIN => [
                'description_admin'
            ],
            self::SCENARIO_UPLOAD => [
                'photographer_comment'
            ],
        ];
        return array_merge(parent::scenarios(), $scenarios);
    }

    public function beforeSave($insert)
    {
        $this->execution_date_from && $this->execution_date_from = date('Y-m-d H:i:s', strtotime($this->execution_date_from));
        $this->execution_date_to && $this->execution_date_to = date('Y-m-d H:i:s', strtotime($this->execution_date_to));
        $this->planing_contact_date && $this->planing_contact_date = date('Y-m-d H:i:s', strtotime($this->planing_contact_date));
        $this->changing_status_date && $this->changing_status_date = date('Y-m-d H:i:s', strtotime($this->changing_status_date));

        if (in_array($this->status, [Constant::ORDER_STATUS_MISSED_CALL_1, Constant::ORDER_STATUS_MISSED_CALL_2]) &&
            ($this->status != $this->getOldAttribute('status'))) {

            $this->changing_status_date = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->execution_date_from && $this->execution_date_from = date('d.m.Y H:i', strtotime($this->execution_date_from));
        $this->execution_date_to && $this->execution_date_to = date('d.m.Y H:i', strtotime($this->execution_date_to));
        $this->planing_contact_date && $this->planing_contact_date = date('d.m.Y H:i', strtotime($this->planing_contact_date));
        $this->changing_status_date && $this->changing_status_date = date('d.m.Y H:i', strtotime($this->changing_status_date));
        parent::afterFind();
    }

    public function validateAddress($attribute, $params)
    {
        $coordinates = GeoHelper::getCoordinates($this->city, $this->street, $this->house);
        if ($coordinates === null) {
            $this->addError('city', 'Неверно указан адрес');
            $this->addError('street', 'Неверно указан адрес');
            $this->addError('house', 'Неверно указан адрес');
            return;
        }
        $this->coordinates = $coordinates;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotographer()
    {
        return $this->hasOne(Photographer::className(), ['id' => 'photographer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(OrderImage::className(), ['order_id' => 'id']);
    }

    public function getStatus()
    {
        return Constant::orderStatuses(true)[$this->status];
    }

    /**
     * @param string $attribute
     * @return string|null
     */
    public function getDate(string $attribute): ?string
    {
        $dateValidator = new DateValidator(['format' => 'php:d.m.Y H:i']);
        return $dateValidator->validate($this->$attribute) ? date('d.m.Y H:i:s', strtotime($this->$attribute)) : null;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->city . ', ' . $this->street . ', ' . $this->house . ', кв. ' . $this->apartment_number;
    }

    /**
     * @return string
     */
    public function getRoomQntType()
    {
        return $this->room_qnt_type ? Constant::qntRoomTypes(true)[$this->room_qnt_type] : null;
    }

    /**
     * @return bool
     */
    public function isReady(): bool
    {
        return in_array($this->status, Constant::readyOrderStatuses());
    }

    /**
     * @param UploadedFile[] $photos
     * @throws \yii\base\Exception
     */
    public function uploadPhotos(array $photos)
    {
        !file_exists(OrderImage::IMAGE_PATH) && mkdir(OrderImage::IMAGE_PATH, '0666', true);

        foreach ($photos as $photo) {
            if ($photo instanceof UploadedFile) {
                $fileName = md5(Yii::$app->security->generateRandomString(16) . time()) . '.' . $photo->extension;
                $imageModel = new OrderImage(['order_id' => $this->id, 'image' => $fileName]);

                $imageModel->save() && $photo->saveAs(OrderImage::IMAGE_PATH . $fileName);
            }
        }
    }

    /**
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function deleteWithImages()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $result = parent::delete();

        if ($result !== false) {
            foreach ($this->images as $image) {
                $image->delete();
            }

            $transaction->commit();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getImagesList()
    {
        $result = [];
        foreach ($this->images as $image) {
            $result[] = $image->getImagePath();
        }

        return $result;
    }

    /**
     * @return string|null
     */
    public function getCoordinatesForMap(): ?string
    {
        return $this->coordinates ? str_replace(' ', ', ', $this->coordinates) : null;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return ($this->execution_date_from ?: '') . ' &mdash; ' . ($this->execution_date_to ?: '');
    }

    /**
     * @return string
     */
    public function getShortPeriod()
    {
        return ($this->execution_date_from ? date('d.m', strtotime($this->execution_date_from)) : '') . ' &mdash; ' . ($this->execution_date_to ? date('d.m', strtotime($this->execution_date_to)) : '');
    }

    public function getColoredNumber()
    {
        return '<span class="' . $this->getStatusColorClass() . '">' . $this->number . '</span>';
    }

    /**
     * @param bool $background
     * @return string
     */
    public function getStatusColorClass(bool $background = false)
    {
        $cssClass = '';
        switch ($this->status) {
            case Constant::ORDER_STATUS_NEW:
                $cssClass = ($background ? 'background-' : '') . 'color-blueStretchyIcon';
                break;
            case Constant::ORDER_STATUS_MISSED_CALL_1:
            case Constant::ORDER_STATUS_MISSED_CALL_2:
                $cssClass = ($background ? 'background-' : '') . 'color-yellowStretchyIcon';
                break;
            case Constant::ORDER_STATUS_PRELIM_CALL:
                $cssClass = ($background ? 'background-' : '') . 'color-violetStretchyIcon';
                break;
            case Constant::ORDER_STATUS_EXECUTING:
                $cssClass = ($background ? 'background-' : '') . 'color-lightGreen';
                break;
            case Constant::ORDER_STATUS_MODERATION:
                $cssClass = ($background ? 'background-' : '') . 'color-green';
                break;
            case Constant::ORDER_STATUS_DOWNLOADED:
                $cssClass = ($background ? 'background-' : '') . ($this->downloaded ? 'color-orange' : 'color-darkGreen');
                break;
            case Constant::ORDER_STATUS_REJECTED:
                $cssClass = ($background ? 'background-' : '') . 'color-redStretchyIcon';
                break;
        }

        return $cssClass;
    }

    /**
     * @return bool
     */
    public function isInNsk(): bool
    {
        if ($this->coordinates) {
            $coordinates = explode(' ', $this->coordinates);
            $latitude = (float)$coordinates[0];
            $longitude = (float)$coordinates[1];

            return !($latitude >= 55.5 || $latitude <= 54.8 || $longitude >= 83.5 || $longitude <= 82.3);
        }
        return false;
    }
}
