<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use Yii;

/**
 * This is the model class for table "administrations".
 *
 * @property int $id
 * @property string|null $identity
 * @property string $name
 * @property string $city
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Identity $entity
 */
class Administrations extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'administrations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['identity'], 'string', 'max' => 36],
            [['name', 'city'], 'string', 'max' => 255],
            [['identity'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::class, 'targetAttribute' => ['identity' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identity' => 'Логин',
            'name' => 'Имя',
            'city' => 'Город',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Identity::class, ['id' => 'identity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::class, ['administration_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotographer()
    {
        return $this->hasOne(Photographer::class, ['administration_id' => 'id']);
    }
}
