<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "photographer".
 *
 * @property int $id
 * @property string|null $identity
 * @property string $fio
 * @property string|null $phone_number
 * @property string|null $address
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Order[] $orders
 * @property Identity $entity
 * @property int $administration_id [int(11)]
 *
 * @property Administrations $administrator
 */
class Photographer extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photographer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'phone_number', 'address'], 'required'],
            [['created_at', 'updated_at', 'administration_id'], 'integer'],
            [['identity'], 'string', 'max' => 36],
            [['fio', 'phone_number', 'address'], 'string', 'max' => 255],
            [['identity'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::className(), 'targetAttribute' => ['identity' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'identity' => 'Логин',
            'fio' => 'ФИО',
            'phone_number' => 'Номер телефона',
            'administration_id' => 'Администратор',
            'address' => 'Адрес',
            'created' => 'Создано',
            'updated' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['photographer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Identity::className(), ['id' => 'identity']);
    }

    /**
     * @return array
     */
    public static function ddl(): array
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'fio');
    }

    /**
     * @return array
     */
    public static function ddlFilter(): array
    {
        $ddl = ['null' => '--Не назначен--'];
        $items = self::find()->all();
        foreach ($items as $item) {
            $ddl[$item->id] = $item->fio;
        }

        return $ddl;
    }

    public function beforeSave($insert)
    {
        $role = Yii::$app->user->identity->getRole();
        if ($role != Constant::ROLE_ADMIN_OF_PHOTOGRAPHER) {
            $this->administration_id = Administrations::findOne(['identity' => Yii::$app->user->id])->id;
        } else {
            $this->administration_id = User::findOne(['identity' => Yii::$app->user->id])->administration_id;
        }
        return parent::beforeSave($insert);
    }

    public function getAdministrator()
    {
        return $this->hasOne(Administrations::class, ['id' => 'administration_id']);
    }
}
