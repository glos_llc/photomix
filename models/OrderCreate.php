<?php


namespace app\models;


use app\components\traits\OrderValidate;
use Yii;

class OrderCreate extends Order
{
    use OrderValidate;

    public function rules()
    {
        return [
            [['apartment_number', 'room_qnt_type', 'zone', 'client_name', 'realtor', 'agency_id', 'number'], 'required'],
            [['coordinates', 'city', 'street', 'house', 'realtor', 'client_name', 'phone_number', 'number',
                'description', 'photographer_comment', 'description_admin'], 'string'],
            [['price', 'price_agency'], 'number'],
            ['checker', 'checked'],

            [['zone', 'room_qnt_type'], 'required', 'on' => self::SCENARIO_CREATE_UPDATE],

            [['planing_contact_date'], 'required', 'when' => function ($model) {
                return in_array($model->status, [Constant::ORDER_STATUS_PRELIM_CALL]);
            }, 'whenClient' => "function (attribute, value) {
                    let ddl = $('#order-status');
                    return (ddl.val() == " . Constant::ORDER_STATUS_PRELIM_CALL . ");
             }"],
            [['execution_date_from', 'execution_date_to', 'photographer_id', 'price'], 'required', 'when' => function ($model) {
                return $model->status == Constant::ORDER_STATUS_EXECUTING;
            }, 'whenClient' => "function (attribute, value) {
                    let ddl = $('#order-status');
                    return ddl.val() == " . Constant::ORDER_STATUS_EXECUTING . ";
             }"],
            [['agency_id', 'photographer_id', 'created_at', 'updated_at', 'zone'], 'integer'],
            [['execution_date_from', 'execution_date_to', 'planing_contact_date', 'changing_status_date'], 'date', 'format' => 'php:d.m.Y H:i'],
            [['execution_date_to'], 'compare', 'compareAttribute' => 'execution_date_from', 'operator' => '>=', 'when' => function ($model) {
                return $this->execution_date_from;
            }],
            [['price', 'price_agency'], 'number'],
            [['number'], 'unique'],
            [['number'], 'default', 'value' => function ($model) {
                $id = (int)Order::find()->select('max(id)')->column()[0] + 1;
                return Yii::$app->getUser()->identity->getRole() === Constant::ROLE_REAL_ESTATE_AGENCY ?
                    Yii::$app->getUser()->identity->agency->name . '_' . $id :
                    'admin_' . $id;
            }],
            [['downloaded'], 'boolean'],
            [['downloaded'], 'default', 'value' => false],
            [['status'], 'default', 'value' => Constant::ORDER_STATUS_NEW],
            [['room_qnt_type'], 'in', 'range' => Constant::qntRoomTypes()],
            [['zone'], 'in', 'range' => Constant::getZoneTypes()],
            [['status'], 'in', 'range' => Constant::orderStatuses()],
            [['status', 'room_qnt_type'], 'string', 'max' => 30],
        ];
    }
}