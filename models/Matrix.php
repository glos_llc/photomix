<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "matrix".
 *
 * @property int $id
 * @property int|null $photographer_id Фотограф
 * @property int|null $agency_id Агенство
 * @property int $one_room_first_zone 1 комнатная, 1 зона
 * @property int $two_room_first_zone 2 комнатная, 1 зона
 * @property int $three_room_first_zone 3 комнатная, 1 зона
 * @property int $four_room_first_zone 4 комнатная, 1 зона
 * @property int $house_first_zone Дом, 1 зона
 * @property int $one_room_second_zone 1 комнатная, 2 зона
 * @property int $two_room_second_zone 2 комнатная, 2 зона
 * @property int $three_room_second_zone 3 комнатная, 2 зона
 * @property int $four_room_second_zone 4 комнатная, 2 зона
 * @property int $house_second_zone Дом, 2 зона
 * @property int $one_room_third_zone 1 комнатная, 3 зона
 * @property int $two_room_third_zone 2 комнатная, 3 зона
 * @property int $three_room_third_zone 3 комнатная, 3 зона
 * @property int $four_room_third_zone 4 комнатная, 3 зона
 * @property int $house_third_zone Дом, 3 зона
 * @property int $one_room_fourth_zone 1 комнатная, 4 зона
 * @property int $two_room_fourth_zone 2 комнатная, 4 зона
 * @property int $three_room_fourth_zone 3 комнатная, 4 зона
 * @property int $four_room_fourth_zone 4 комнатная, 4 зона
 * @property int $house_fourth_zone Дом, 4 зона
 * @property int $one_room_fifth_zone 1 комнатная, 5 зона
 * @property int $two_room_fifth_zone 2 комнатная, 5 зона
 * @property int $three_room_fifth_zone 3 комнатная, 5 зона
 * @property int $four_room_fifth_zone 4 комнатная, 5 зона
 * @property int $house_fifth_zone Дом, 5 зона
 * @property int $created_at Дата создания
 * @property int|null $updated_at Дата обновления
 *
 * @property Agency $agency
 * @property Photographer $photograph
 */
class Matrix extends \yii\db\ActiveRecord
{
    public $administration;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matrix';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photographer_id', 'agency_id', 'one_room_first_zone', 'two_room_first_zone', 'three_room_first_zone',
                'four_room_first_zone', 'house_first_zone', 'one_room_second_zone', 'two_room_second_zone',
                'three_room_second_zone', 'four_room_second_zone', 'house_second_zone', 'one_room_third_zone',
                'two_room_third_zone', 'three_room_third_zone', 'four_room_third_zone', 'house_third_zone',
                'one_room_fourth_zone', 'two_room_fourth_zone', 'three_room_fourth_zone', 'four_room_fourth_zone',
                'house_fourth_zone', 'one_room_fifth_zone', 'two_room_fifth_zone', 'three_room_fifth_zone',
                'four_room_fifth_zone', 'house_fifth_zone', 'created_at', 'updated_at'], 'integer'],

            [['one_room_first_zone', 'two_room_first_zone', 'three_room_first_zone', 'four_room_first_zone',
                'house_first_zone', 'one_room_second_zone', 'two_room_second_zone', 'three_room_second_zone',
                'four_room_second_zone', 'house_second_zone', 'one_room_third_zone', 'two_room_third_zone',
                'three_room_third_zone', 'four_room_third_zone', 'house_third_zone', 'one_room_fourth_zone',
                'two_room_fourth_zone', 'three_room_fourth_zone', 'four_room_fourth_zone', 'house_fourth_zone',
                'one_room_fifth_zone', 'two_room_fifth_zone', 'three_room_fifth_zone', 'four_room_fifth_zone',
                'house_fifth_zone'], 'required'],

            ['photographer_id', 'unique', 'message' => 'этот фотограф уже сущевствует'],
            [['photographer_id'], 'validateAgencyAndPhotograph'],
            [['photographer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Photographer::class,
                'targetAttribute' => ['photographer_id' => 'id']],

            ['photographer_id', 'required', 'when' => function ($model) {
                return $model->agency_id == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#agency_id').val() == '';
                }"
                , 'message' => 'Заполните что-то одно. Либо фотографа, либо агента'],

            ['agency_id', 'unique', 'message' => 'этот агент уже сущевствует'],
            [['agency_id'], 'validateAgencyAndPhotograph'],
            [['agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::class,
                'targetAttribute' => ['agency_id' => 'id']],

            ['agency_id', 'required', 'when' => function ($model) {
                return $model->photographer_id == '';
            }, 'whenClient' => "function (attribute, value) {
                    return $('#photographer_id').val() == '';
                }",
                'message' => 'Заполните что-то одно. Либо фотографа, либо агента'],
        ];
    }

    /**
     * @return bool
     */
    public function validateAgencyAndPhotograph()
    {
        if (($this->agency_id != '') && ($this->photographer_id != '')) {
            $this->addError('photographer_id', 'Нельзя выбрать и фотографа и агента, выберите одного');
            return false;
        }

        return true;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photographer_id' => 'Фотограф',
            'agency_id' => 'Агенство',
            'one_room_first_zone' => '1 комнатная, 1 зона',
            'two_room_first_zone' => '2 комнатная, 1 зона',
            'three_room_first_zone' => '3 комнатная, 1 зона',
            'four_room_first_zone' => '4 комнатная, 1 зона',
            'house_first_zone' => 'Дом, 1 зона',
            'one_room_second_zone' => '1 комнатная, 2 зона',
            'two_room_second_zone' => '2 комнатная, 2 зона',
            'three_room_second_zone' => '3 комнатная, 2 зона',
            'four_room_second_zone' => '4 комнатная, 2 зона',
            'house_second_zone' => 'Дом, 2 зона',
            'one_room_third_zone' => '1 комнатная, 3 зона',
            'two_room_third_zone' => '2 комнатная, 3 зона',
            'three_room_third_zone' => '3 комнатная, 3 зона',
            'four_room_third_zone' => '4 комнатная, 3 зона',
            'house_third_zone' => 'Дом, 3 зона',
            'one_room_fourth_zone' => '1 комнатная, 4 зона',
            'two_room_fourth_zone' => '2 комнатная, 4 зона',
            'three_room_fourth_zone' => '3 комнатная, 4 зона',
            'four_room_fourth_zone' => '4 комнатная, 4 зона',
            'house_fourth_zone' => 'Дом, 4 зона',
            'one_room_fifth_zone' => '1 комнатная, 5 зона',
            'two_room_fifth_zone' => '2 комнатная, 5 зона',
            'three_room_fifth_zone' => '3 комнатная, 5 зона',
            'four_room_fifth_zone' => '4 комнатная, 5 зона',
            'house_fifth_zone' => 'Дом, 5 зона',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'administration' => 'Администратор',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::class, ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotograph()
    {
        return $this->hasOne(Photographer::class, ['id' => 'photographer_id']);
    }

    /**
     * @return array
     */
    public function getPhotographs()
    {
        return ArrayHelper::map(Photographer::find()->asArray()->all(), 'id', 'fio');
    }

    /**
     * @return array
     */
    public function getAgencys()
    {
        return ArrayHelper::map(Agency::find()->asArray()->all(), 'id', 'name');
    }
}
