<?php

namespace app\models;

abstract class Constant
{
    const ROLE_ADMIN = 'admin';
    const ROLE_REAL_ESTATE_AGENCY = 'real-estate-agency';
    const ROLE_ADMIN_OF_PHOTOGRAPHER = 'admin-of-photographer';
    const ROLE_PHOTOGRAPHER = 'photographer';
    const ROLE_PHOTO_MODERATOR = 'photo-moderator';
    const ROLE_ADMINISTRATION = 'administration';

    const YMAP_ICON_PRESETS = [
        'islands#blueStretchyIcon', 'islands#darkGreenStretchyIcon', 'islands#redStretchyIcon', 'islands#violetStretchyIcon',
        'islands#darkOrangeStretchyIcon', 'islands#blackStretchyIcon', 'islands#nightStretchyIcon', 'islands#yellowStretchyIcon',
        'islands#darkBlueStretchyIcon', 'islands#greenStretchyIcon', 'islands#pinkStretchyIcon', 'islands#orangeStretchyIcon',
        'islands#grayStretchyIcon', 'islands#lightBlueStretchyIcon', 'islands#brownStretchyIcon', 'islands#oliveStretchyIcon'
    ];

    const YMAP_ICON_PRESETS_COLOR_CORRELATION = [
        'islands#blueStretchyIcon' => 'color-blueStretchyIcon',
        'islands#darkGreenStretchyIcon' => 'color-darkGreenStretchyIcon',
        'islands#redStretchyIcon' => 'color-redStretchyIcon',
        'islands#violetStretchyIcon' => 'color-violetStretchyIcon',
        'islands#darkOrangeStretchyIcon' => 'color-darkOrangeStretchyIcon',
        'islands#blackStretchyIcon' => 'color-blackStretchyIcon',
        'islands#nightStretchyIcon' => 'color-nightStretchyIcon',
        'islands#yellowStretchyIcon' => 'color-yellowStretchyIcon',
        'islands#darkBlueStretchyIcon' => 'color-darkBlueStretchyIcon',
        'islands#greenStretchyIcon' => 'color-greenStretchyIcon',
        'islands#pinkStretchyIcon' => 'color-pinkStretchyIcon',
        'islands#orangeStretchyIcon' => 'color-orangeStretchyIcon',
        'islands#grayStretchyIcon' => 'color-grayStretchyIcon',
        'islands#lightBlueStretchyIcon' => 'color-lightBlueStretchyIcon',
        'islands#brownStretchyIcon' => 'color-brownStretchyIcon',
        'islands#oliveStretchyIcon' => 'color-oliveStretchyIcon',
    ];

    public static function roles(bool $withDescription = false): array
    {
        return $withDescription ? [
            self::ROLE_ADMIN => 'Админ',
            self::ROLE_ADMINISTRATION => 'Администратор',
            self::ROLE_REAL_ESTATE_AGENCY => 'Агенство недвижимости',
            self::ROLE_ADMIN_OF_PHOTOGRAPHER => 'Администратор фотографов',
            self::ROLE_PHOTOGRAPHER => 'Фотограф',
            self::ROLE_PHOTO_MODERATOR => 'Модератор фотографий',
        ] :
            [
                self::ROLE_ADMIN,
                self::ROLE_REAL_ESTATE_AGENCY,
                self::ROLE_ADMIN_OF_PHOTOGRAPHER,
                self::ROLE_PHOTOGRAPHER,
                self::ROLE_PHOTO_MODERATOR,
            ];
    }

    public static function userRoles(bool $withDescription = false): array
    {
        return $withDescription ? [
            self::ROLE_ADMIN_OF_PHOTOGRAPHER => 'Администратор фотографов',
            self::ROLE_PHOTO_MODERATOR => 'Модератор фотографий',
        ] :
            [
                self::ROLE_ADMIN_OF_PHOTOGRAPHER,
                self::ROLE_PHOTO_MODERATOR,
            ];
    }

    const ROOM_1_STUDIO = '1-room-studio';
    const ROOM_2 = '2-room';
    const ROOM_3 = '3-room';
    const ROOM_4 = '4-room';
    const ROOM_HOUSE = 'house';

    public static function qntRoomTypes(bool $withDescription = false): array
    {
        return $withDescription ?
            [
                self::ROOM_1_STUDIO => '1-комнатная студия',
                self::ROOM_2 => '2-комнатная квартира',
                self::ROOM_3 => '3-комнатная квартира',
                self::ROOM_4 => '4-комнатная квартира',
                self::ROOM_HOUSE => 'коттедж/дом',
            ] :
            [
                // Порядок не менять
                self::ROOM_1_STUDIO,
                self::ROOM_2,
                self::ROOM_3,
                self::ROOM_4,
                self::ROOM_HOUSE,
            ];
    }

    const ZONE_1 = '1';
    const ZONE_2 = '2';
    const ZONE_3 = '3';
    const ZONE_4 = '4';
    const ZONE_5 = '5';

    public static function getZoneTypes(bool $withDescription = false): array
    {
        return $withDescription ?
            [
                self::ZONE_1 => '1 зона',
                self::ZONE_2 => '2 зона',
                self::ZONE_3 => '3 зона',
                self::ZONE_4 => '4 зона',
                self::ZONE_5 => '5 зона',
            ] :
            [
                // Порядок не менять
                self::ZONE_1,
                self::ZONE_2,
                self::ZONE_3,
                self::ZONE_4,
                self::ZONE_5,
            ];
    }

    const ORDER_STATUS_NEW = 'status-new';
    const ORDER_STATUS_PRELIM_CALL = 'status-prelim-call';
    const ORDER_STATUS_MISSED_CALL_1 = 'status-missed-call-1';
    const ORDER_STATUS_MISSED_CALL_2 = 'status-missed-call-2';
    const ORDER_STATUS_EXECUTING = 'status-executing';
    const ORDER_STATUS_MODERATION = 'status-moderation';
    const ORDER_STATUS_DOWNLOADED = 'status-downloaded';
    const ORDER_STATUS_REJECTED = 'status-rejected';
    const ORDER_STATUS_FAILED = 'status-failed';

    public static function orderStatuses(bool $withDescription = false): array
    {
        return $withDescription ?
            [
                self::ORDER_STATUS_NEW => 'Новая',
                self::ORDER_STATUS_PRELIM_CALL => 'Предварительный созвон',
                self::ORDER_STATUS_MISSED_CALL_1 => 'Недозвон 1',
                self::ORDER_STATUS_MISSED_CALL_2 => 'Недозвон 2',
                self::ORDER_STATUS_EXECUTING => 'На исполнении',
                self::ORDER_STATUS_MODERATION => 'На модерации',
                self::ORDER_STATUS_DOWNLOADED => 'Фото загружены',
                self::ORDER_STATUS_REJECTED => 'Отказ клиента',
                self::ORDER_STATUS_FAILED => 'Ложная',
            ] :
            [
                self::ORDER_STATUS_NEW,
                self::ORDER_STATUS_PRELIM_CALL,
                self::ORDER_STATUS_MISSED_CALL_1,
                self::ORDER_STATUS_MISSED_CALL_2,
                self::ORDER_STATUS_EXECUTING,
                self::ORDER_STATUS_MODERATION,
                self::ORDER_STATUS_DOWNLOADED,
                self::ORDER_STATUS_REJECTED,
                self::ORDER_STATUS_FAILED,
            ];
    }

    public static function notReadyOrderStatuses(bool $withDescription = false): array
    {
        return $withDescription ?
            [
                self::ORDER_STATUS_NEW => 'Новая',
                self::ORDER_STATUS_PRELIM_CALL => 'Предварительный созвон',
                self::ORDER_STATUS_MISSED_CALL_1 => 'Недозвон 1',
                self::ORDER_STATUS_MISSED_CALL_2 => 'Недозвон 2',
                self::ORDER_STATUS_EXECUTING => 'На исполнении',
                self::ORDER_STATUS_MODERATION => 'На модерации',
            ] :
            [
                self::ORDER_STATUS_NEW,
                self::ORDER_STATUS_PRELIM_CALL,
                self::ORDER_STATUS_MISSED_CALL_1,
                self::ORDER_STATUS_MISSED_CALL_2,
                self::ORDER_STATUS_EXECUTING,
                self::ORDER_STATUS_MODERATION,
            ];
    }

    public static function readyOrderStatuses(bool $withDescription = false): array
    {
        return $withDescription ?
            [
                self::ORDER_STATUS_DOWNLOADED => 'Фото загружены',
                self::ORDER_STATUS_REJECTED => 'Отказ клиента',
                self::ORDER_STATUS_FAILED => 'Ложная заявка',
            ] :
            [
                self::ORDER_STATUS_DOWNLOADED,
                self::ORDER_STATUS_REJECTED,
                self::ORDER_STATUS_FAILED,
            ];
    }

    public static function orderStatusesDDLByRole($role, bool $withDescription = false): array
    {
        switch ($role) {
            case Constant::ROLE_ADMIN_OF_PHOTOGRAPHER:
                return $withDescription ?
                    [
                        self::ORDER_STATUS_EXECUTING => 'На исполнении',
                        self::ORDER_STATUS_MODERATION => 'На модерации',
                        self::ORDER_STATUS_DOWNLOADED => 'Фото загружены',
                    ] :
                    [
                        self::ORDER_STATUS_EXECUTING,
                        self::ORDER_STATUS_MODERATION,
                        self::ORDER_STATUS_DOWNLOADED,
                    ];
        }

        return [];
    }
}