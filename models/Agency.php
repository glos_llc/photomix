<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "real_estate_agency".
 *
 * @property int $id
 * @property string|null $identity
 * @property string $name
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Order[] $orders
 * @property Identity $entity
 * @property int $administration_id [int(11)]
 * @property Administrations $administrations
 */
class Agency extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    public $city;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'real_estate_agency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'administration_id'], 'integer'],
            [['identity'], 'string', 'max' => 36],
            [['name'], 'string', 'max' => 255],
            [['identity'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::className(), 'targetAttribute' => ['identity' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'identity' => 'Логин',
            'name' => 'Название',
            'created' => 'Создано',
            'updated' => 'Обновлено',
            'administration_id' => 'Администратор',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdministrations()
    {
        return $this->hasOne(Administrations::class, ['id' => 'administration_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Identity::class, ['id' => 'identity']);
    }

    /**
     * @return array
     */
    public static function ddl(): array
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function beforeSave($insert)
    {
        if (empty($this->administration_id) || !isset($this->administration_id)) {
            $this->administration_id = Administrations::findOne(['identity' => Yii::$app->user->id])->id;
        }
        return parent::beforeSave($insert);
    }
}
