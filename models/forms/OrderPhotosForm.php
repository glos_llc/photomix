<?php


namespace app\models\forms;


use app\components\behaviors\Response;
use app\models\Order;
use app\models\OrderImage;

class OrderPhotosForm extends StatusForm
{
    /**
     * @return array
     */
    public function getPhotos()
    {
        $order = Order::findOne(['rrId' => $this->rrId]);

        if (!$order) {
            return Response::responseError('По такому id нет заявки');
        }

        $orderPhotos = OrderImage::find()->where(['order_id' => $order->id])->all();

        if (!$orderPhotos) {
            return Response::responseError('У заявки нет фотографий');
        }

        if ($order->status != 'status-downloaded') {
            return Response::responseError('Статус заявки должен быть "Фото загружены"');
        }

        $result = [];
        foreach ($orderPhotos as $photo) {
            array_push($result, $_SERVER['SERVER_NAME'] . $photo->getImagePath());
        }

        return Response::succeededWithData(['photos' => $result]);
    }
}