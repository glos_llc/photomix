<?php


namespace app\models\forms;


use app\components\behaviors\Response;
use app\models\{Constant, labels\OrderLabel, Order};
use yii\base\Model;
use yii\helpers\ArrayHelper;

class StatusForm extends Model
{
    public $rrId;

    public function rules()
    {
        return [
            ['rrId', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return OrderLabel::getAttributeLabels();
    }

    /**
     * @return array
     */
    public function getStatus()
    {
        $order = Order::findOne(['rrId' => $this->rrId]);

        if (!$order) {
            return Response::responseError('По такому id нет заявки');
        }

        return Response::succeededWithData(['status' => $this->valueStatus($order->status)]);

    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public function valueStatus($status)
    {
        return ArrayHelper::getValue(Constant::orderStatuses(true), $status);
    }
}