<?php


namespace app\models\forms;


use app\components\behaviors\Response;
use app\models\{Agency, Constant, Order};
use yii\helpers\ArrayHelper;

class OrderInfoForm extends StatusForm
{
    /**
     * @return array
     */
    public function getOrder()
    {
        $order = Order::findOne(['rrId' => $this->rrId]);

        if (!$order) {
            return Response::responseError('По такому id нет заявки');
        }

        return Response::succeededWithData([
            'rrId' => $order->rrId,
            'number' => $order->number,
            'agency' => Agency::findOne(['id' => $order->agency_id])->name,
            'realtor' => $order->realtor,
            'status' => $this->valueStatus($order->status),
            'room_qnt_type' => ArrayHelper::getValue(Constant::qntRoomTypes(true), $order->room_qnt_type),
            'zone' => $order->zone,
            'address' => $order->city . ' ' . $order->street . ' ' . $order->house . ' ' . $order->apartment_number,
            'description' => $order->description,
            'description_admin' => $order->description_admin,
            'client_name' => $order->client_name,
            'phone_number' => $order->phone_number,
            'execution_date' => $this->getExecutionDate($order),
            'planing_contact_date' => $order->planing_contact_date,
            'created_at' => $order->created_at
        ]);
    }

    /**
     * @param $order
     *
     * @return string|null
     */
    private function getExecutionDate($order)
    {
        if (!$order->execution_date_from && !$order->execution_date_to){
            return null;
        }else if ($order->execution_date_from && $order->execution_date_to){
            return 'Дата исполнения с ' . $order->execution_date_from . 'Дата исполнения по ' . $order->execution_date_to;
        }else if (!$order->execution_date_from){
            return 'Дата исполнения по ' . $order->execution_date_to;
        }else if (!$order->execution_date_to){
            return 'Дата исполнения с ' . $order->execution_date_from;
        }
    }
}