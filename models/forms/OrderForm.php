<?php


namespace app\models\forms;


use app\components\behaviors\Response;
use app\helpers\AddressHelper;
use app\models\{Constant, Order};
use app\models\labels\OrderLabel;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class OrderForm extends Model
{
    public $rrId;
    public $number;
    public $city;
    public $street;
    public $house;
    public $apartment_number;
    public $room_qnt_type;
    public $client_name;
    public $phone_number;
    public $agency_id;
    public $description;
    public $coordinates;
    public $updated_at;
    public $created_at;
    public $status;
    public $realtor;

    public function rules()
    {
        return [
            [['rrId', 'coordinates', 'room_qnt_type', 'number', 'realtor'], 'required'],
            ['coordinates', 'validateCoordinates'],
            ['client_name', 'validateClientName', 'skipOnEmpty' => false],
            ['coordinates', 'string'],
            [['rrId', 'number', 'city', 'street', 'house', 'apartment_number', 'room_qnt_type', 'client_name',
                'phone_number', 'agency_id', 'description', 'coordinates', 'status', 'realtor'], 'string'],
            ['room_qnt_type', 'validateRoom'],
            ['rrId', 'checkUnique'],
            ['rrId', 'checkUniqueNumber'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return OrderLabel::getAttributeLabels();
    }

    public function checkUnique(): bool
    {
        if (Order::findOne(['rrId' => $this->rrId])) {
            $this->addError('rrId', 'Такой id уже создан');
            return false;
        }

        return true;
    }

    public function checkUniqueNumber(): bool
    {
        if (Order::findOne(['number' => $this->number])) {
            $this->addError('number', 'Такой уже существует');
            return false;
        }

        return true;
    }

    public function validateRoom(): bool
    {
        if (!ArrayHelper::isIn($this->room_qnt_type, Constant::qntRoomTypes())) {
            $this->addError('room_qnt_type', 'Такое значение отсутсвует');
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function validateCoordinates(): bool
    {
        $spaceInString = mb_stripos($this->coordinates, ' ');
        if ($spaceInString === false) {
            $this->addError('coordinates', 'Формат должен быть: 55.047404, 82.906164');
            return false;
        }

        return true;
    }

    /**
     * validate client name
     */
    public function validateClientName()
    {
        if (!$this->client_name) {
            $this->client_name = $this->realtor;
        }
    }

    public function createOrder()
    {
        $this->created_at = time();
        $this->updated_at = time();
        $this->status = 'status-new';
        $dataForInsert = (array)$this->attributes;

        try {
            \Yii::$app->db->createCommand()->insert('order', $dataForInsert)
                ->execute();
        } catch (Exception $e) {
            return Response::responseError('Произошла ошибка при сохранении');
        }

        return Response::succeededWithData('Заявка успешно создана');
    }

    /**
     * Get address from yandex xml
     *
     * @return bool
     */
    public function parseAddress(): bool
    {
        $addressHelper = new AddressHelper();
        $yandexXml = $addressHelper->getAddress($this->coordinates);
        if ($yandexXml) {
            return $addressHelper->parseAddress($yandexXml, $this->city, $this->street, $this->house);
        }
        return false;
    }
}