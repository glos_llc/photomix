<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "order_image".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string $image
 * @property string|null $description
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Order $order
 */
class OrderImage extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    const IMAGE_PATH = 'uploads/orders/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['image'], 'required'],
            [['image', 'description'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'image' => 'Image',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return Url::base() . '/' . self::IMAGE_PATH . $this->image;
    }

    public function delete()
    {
        $file = $this->image;
        $result = parent::delete();

        if ($result !== false) {
            @unlink(self::IMAGE_PATH . $file);
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isImage(): bool
    {
        $extensions = ['.jpg', '.jpeg', '.png'];

        foreach ($extensions as $extension) {
            if(stripos($this->image, $extension) !== false) {
                return true;
            }
        }

        return false;
    }
}
