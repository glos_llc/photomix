<?php
namespace app\console\controllers;

use app\models\Order;
use yii\console\Controller;
use yii\helpers\Console;

class AppController extends Controller
{
    const DELETE_PERIOD = '';

    /**
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteOldOrders()
    {
        $orders = Order::find()->where(['<', 'updated_at', strtotime('-3 month', time())])->all();

        foreach ($orders as $order) {
            $order->deleteWithImages();
        }

        Console::stdout(Console::ansiFormat('Orders deleted' . PHP_EOL, [Console::FG_GREEN]));
    }
}