<?php

namespace app\console\controllers;

use app\models\Constant;
use yii\console\Controller;
use Yii;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole(Constant::ROLE_ADMIN);
        $adminPhoto = $auth->createRole(Constant::ROLE_ADMIN_OF_PHOTOGRAPHER);
        $agency = $auth->createRole(Constant::ROLE_REAL_ESTATE_AGENCY);
        $moderator = $auth->createRole(Constant::ROLE_PHOTO_MODERATOR);
        $photographer = $auth->createRole(Constant::ROLE_PHOTOGRAPHER);

        $auth->add($admin);
        $auth->add($adminPhoto);
        $auth->add($agency);
        $auth->add($moderator);
        $auth->add($photographer);

        $auth->addChild($adminPhoto, $moderator);
        $auth->addChild($adminPhoto, $photographer);

        $auth->addChild($admin, $adminPhoto);
        $auth->addChild($admin, $agency);

        $auth->assign($admin, '7fc43573-c18d-4978-9580-a0983a5c6f50');

        Console::stdout(Console::ansiFormat('Roles initiated', [Console::FG_GREEN]));
    }

    public function actionAddAdministration()
    {
        $auth = Yii::$app->authManager;

        $administration = $auth->createRole(Constant::ROLE_ADMINISTRATION);
        $auth->add($administration);
        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $administration);

        Console::stdout(Console::ansiFormat('Role administration initiated', [Console::FG_GREEN]));
    }
}