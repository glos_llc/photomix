<?php

use yii\db\Migration;

/**
 * Class m201022_040003_alter_zone_order_table
 */
class m201022_040003_alter_zone_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'zone', $this->tinyInteger()->null()->comment('Зона'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'zone');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_040003_alter_zone_order_table cannot be reverted.\n";

        return false;
    }
    */
}
