<?php

use yii\db\Migration;

/**
 * Class m201021_043240_alter_history_call_order_table
 */
class m201021_043240_alter_history_call_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'call_history', $this->text()->null()->comment('История звонка'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'call_history');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_043240_alter_history_call_order_table cannot be reverted.\n";

        return false;
    }
    */
}
