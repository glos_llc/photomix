<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%matrix}}`.
 */
class m201021_073918_create_matrix_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%matrix}}', [
            'id' => $this->primaryKey(),
            'photographer_id' => $this->integer()->null()->unique()->comment('Фотограф'),
            'agency_id' => $this->integer()->null()->unique()->comment('Агенство'),

            'one_room_first_zone' => $this->integer()->notNull()->comment('1 комнатная, 1 зона'),
            'two_room_first_zone' => $this->integer()->notNull()->comment('2 комнатная, 1 зона'),
            'three_room_first_zone' => $this->integer()->notNull()->comment('3 комнатная, 1 зона'),
            'four_room_first_zone' => $this->integer()->notNull()->comment('4 комнатная, 1 зона'),
            'house_first_zone' => $this->integer()->notNull()->comment('Дом, 1 зона'),

            'one_room_second_zone' => $this->integer()->notNull()->comment('1 комнатная, 2 зона'),
            'two_room_second_zone' => $this->integer()->notNull()->comment('2 комнатная, 2 зона'),
            'three_room_second_zone' => $this->integer()->notNull()->comment('3 комнатная, 2 зона'),
            'four_room_second_zone' => $this->integer()->notNull()->comment('4 комнатная, 2 зона'),
            'house_second_zone' => $this->integer()->notNull()->comment('Дом, 2 зона'),

            'one_room_third_zone' => $this->integer()->notNull()->comment('1 комнатная, 3 зона'),
            'two_room_third_zone' => $this->integer()->notNull()->comment('2 комнатная, 3 зона'),
            'three_room_third_zone' => $this->integer()->notNull()->comment('3 комнатная, 3 зона'),
            'four_room_third_zone' => $this->integer()->notNull()->comment('4 комнатная, 3 зона'),
            'house_third_zone' => $this->integer()->notNull()->comment('Дом, 3 зона'),

            'one_room_fourth_zone' => $this->integer()->notNull()->comment('1 комнатная, 4 зона'),
            'two_room_fourth_zone' => $this->integer()->notNull()->comment('2 комнатная, 4 зона'),
            'three_room_fourth_zone' => $this->integer()->notNull()->comment('3 комнатная, 4 зона'),
            'four_room_fourth_zone' => $this->integer()->notNull()->comment('4 комнатная, 4 зона'),
            'house_fourth_zone' => $this->integer()->notNull()->comment('Дом, 4 зона'),

            'one_room_fifth_zone' => $this->integer()->notNull()->comment('1 комнатная, 5 зона'),
            'two_room_fifth_zone' => $this->integer()->notNull()->comment('2 комнатная, 5 зона'),
            'three_room_fifth_zone' => $this->integer()->notNull()->comment('3 комнатная, 5 зона'),
            'four_room_fifth_zone' => $this->integer()->notNull()->comment('4 комнатная, 5 зона'),
            'house_fifth_zone' => $this->integer()->notNull()->comment('Дом, 5 зона'),

            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->null()->comment('Дата обновления'),
        ]);

        $this->addForeignKey('matrix_photographer_id', 'matrix', 'photographer_id',
            'photographer', 'id');

        $this->addForeignKey('matrix_agency_id', 'matrix', 'agency_id',
            'real_estate_agency', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%matrix}}');
    }
}
