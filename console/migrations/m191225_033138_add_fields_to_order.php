<?php

use yii\db\Migration;

/**
 * Class m191225_033138_add_fields_to_order
 */
class m191225_033138_add_fields_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\Order::tableName(), 'price_agency', $this->float());
        $this->addColumn(\app\models\Order::tableName(), 'apartment_number', $this->string());
        $this->addColumn(\app\models\Order::tableName(), 'description_admin', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(\app\models\Order::tableName(), 'price_agency');
        $this->dropColumn(\app\models\Order::tableName(), 'apartment_number');
        $this->dropColumn(\app\models\Order::tableName(), 'description_admin');
    }
}
