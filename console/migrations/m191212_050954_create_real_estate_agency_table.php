<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%real_estate_agency}}`.
 */
class m191212_050954_create_real_estate_agency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%real_estate_agency}}', [
            'id' => $this->primaryKey(),
            'identity' => $this->string(36),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (identity) REFERENCES identity(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%real_estate_agency}}');
    }
}
