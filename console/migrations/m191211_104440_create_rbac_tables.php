<?php

use yii\db\Migration;

/**
 * Class m191211_104440_create_rbac_tables
 */
class m191211_104440_create_rbac_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Запускаем миграцию для создания rbac-таблиц (yii migrate --migrationPath=@yii/rbac/migrations)
        (new \yii\console\controllers\MigrateController('migrate', Yii::$app))
            ->runAction('up', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      \yii\helpers\Console::stdout('Не получилось');
      die;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191211_104440_create_rbac_tables cannot be reverted.\n";

        return false;
    }
    */
}
