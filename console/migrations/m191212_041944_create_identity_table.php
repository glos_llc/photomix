<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%identity}}`.
 */
class m191212_041944_create_identity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('identity', [
            'id' => $this->string(36),
            'login' => $this->string(),
            'password' => $this->string(),
            'status' => $this->string(10),
            'auth_key' => $this->string(100),
            'PRIMARY KEY (`id`)'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');

        $this->insert('identity',[
            'id' => '7fc43573-c18d-4978-9580-a0983a5c6f50',
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('8*4N_%asC01_4'),
            'status' => \app\models\Identity::STATUS_ACTIVE,
            'auth_key' => Yii::$app->security->generateRandomString(36),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%identity}}');
    }
}
