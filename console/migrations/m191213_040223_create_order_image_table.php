<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_image}}`.
 */
class m191213_040223_create_order_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_image}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'image' => $this->string()->notNull(),
            'description' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (order_id) REFERENCES {{%order}}(id) ON UPDATE CASCADE ON DELETE SET NULL'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_image}}');
    }
}
