<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%photographer}}`.
 */
class m191212_050403_create_photographer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photographer}}', [
            'id' => $this->primaryKey(),
            'identity' => $this->string(36),
            'fio' => $this->string()->notNull(),
            'phone_number' => $this->string(),
            'address' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (identity) REFERENCES identity(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%photographer}}');
    }
}
