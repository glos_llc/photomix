<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m191212_052032_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->notNull(),
            'agency_id' => $this->integer()->notNull(),
            'realtor' => $this->string(),
            'status' => $this->string(30),
            'room_qnt_type' => $this->string(30),
            'city' => $this->string()->notNull(),
            'street' => $this->string()->notNull(),
            'house' => $this->string()->notNull(),
            'coordinates' => $this->string(),
            'description' => $this->text(),
            'client_name' => $this->string(),
            'phone_number' => $this->string(),
            'execution_date_from' => $this->dateTime(),
            'execution_date_to' => $this->dateTime(),
            'planing_contact_date' => $this->dateTime()->defaultValue(null),
            'changing_status_date' => $this->dateTime()->defaultValue(null),
            'price' => $this->float(),
            'photographer_id' => $this->integer(),
            'photographer_comment' => $this->text(),
            'downloaded' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (agency_id) REFERENCES {{%real_estate_agency}}(id) ON UPDATE CASCADE ON DELETE CASCADE',
            'FOREIGN KEY (photographer_id) REFERENCES {{%photographer}}(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}
