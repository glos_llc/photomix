<?php

use yii\db\Migration;

/**
 * Class m210113_062138_added_administration_id
 */
class m210113_062138_added_administration_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'administration_id', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('photographer', 'administration_id', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('real_estate_agency', 'administration_id', $this->integer()->notNull()->defaultValue(1));

        $this->alterColumn('user', 'administration_id', $this->integer()->notNull());
        $this->alterColumn('photographer', 'administration_id', $this->integer()->notNull());
        $this->alterColumn('real_estate_agency', 'administration_id', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'administration_id');
        $this->dropColumn('photographer', 'administration_id');
        $this->dropColumn('real_estate_agency', 'administration_id');
    }
}
