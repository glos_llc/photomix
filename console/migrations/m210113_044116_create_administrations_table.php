<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%administrations}}`.
 */
class m210113_044116_create_administrations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%administrations}}', [
            'id' => $this->primaryKey(),
            'identity' => $this->string(36),
            'name' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (identity) REFERENCES identity(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');

        $this->insert('identity', [
            'id' => '9836f8cd-0b73-45d1-ab95-eaafd8742fd8',
            'login' => 'admin_nsk',
            'password' => '$2y$13$oS5T65yqUEAN00MXwtuZ0efh2AMvdiCbo6NjPNGFl1eu4qkjtNIQm',
            'status' => 'A',
            'auth_key' => 'panze9PLmDXV061DVoTTumSs1DfnflouEtqp',
        ]);

        $this->insert('auth_assignment', [
            'item_name' => 'administration',
            'user_id' => '9836f8cd-0b73-45d1-ab95-eaafd8742fd8',
            'created_at' => time(),
        ]);

        $this->insert('administrations', [
            'id' => '1',
            'identity' => '9836f8cd-0b73-45d1-ab95-eaafd8742fd8',
            'name' => 'Администратор Новосибирск',
            'city' => 'Новосибирск',
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%administrations}}');

        $this->delete('auth_assignment', ['user_id' => '9836f8cd-0b73-45d1-ab95-eaafd8742fd8']);
        $this->delete('identity', ['id' => '9836f8cd-0b73-45d1-ab95-eaafd8742fd8']);
    }
}
