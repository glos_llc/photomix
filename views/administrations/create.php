<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administrations */

$this->title = 'Создать администратора';
$this->params['breadcrumbs'][] = ['label' => 'Администраторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="administrations-create">

    <?= $this->render('_form', [
        'model' => $model,
        'identity' => $identity
    ]) ?>

</div>
