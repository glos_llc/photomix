<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administrations */

$this->title = 'Обновить администратора: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Администраторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="administrations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'identity' => $identity
    ]) ?>

</div>
