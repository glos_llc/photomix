<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matrix */

$this->title = 'Создать матрицу';
$this->params['breadcrumbs'][] = ['label' => 'Матрицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
