<?php

use app\models\{Administrations, Agency, Photographer};
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Matrix */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Матрицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="matrix-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'photographer_id',
                'value' => function ($model) {
                    if (empty(Photographer::findOne(['id' => $model->photographer_id])->fio)) {
                        return 'Не задан';
                    } else {
                        return Photographer::findOne(['id' => $model->photographer_id])->fio;
                    }
                }
            ],
            [
                'attribute' => 'agency_id',
                'value' => function ($model) {
                    if (empty(Agency::findOne(['id' => $model->agency_id])->name)) {
                        return 'Не задан';
                    } else {
                        return Agency::findOne(['id' => $model->agency_id])->name;
                    }
                }
            ],
            [
                'attribute' => 'administration',
                'value' => function ($model) {
                    if (is_null($model->photographer_id)){
                        return Administrations::find()->joinWith('agency')->where(['real_estate_agency.id' =>$model->agency_id])->one()->name;
                    }else{
                        return Administrations::find()->joinWith('photographer')->where(['photographer.id' =>$model->photographer_id])->one()->name;
                    }
                },
                'filter' => false,
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
        ],
    ]) ?>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">1 зона</th>
            <th scope="col">2 зона</th>
            <th scope="col">3 зона</th>
            <th scope="col">4 зона</th>
            <th scope="col">5 зона</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1ком</th>
            <td><?= $model->one_room_first_zone ?></td>
            <td><?= $model->one_room_second_zone ?></td>
            <td><?= $model->one_room_third_zone ?></td>
            <td><?= $model->one_room_fourth_zone ?></td>
            <td><?= $model->one_room_fifth_zone ?></td>
        </tr>
        <tr>
            <th scope="row">2ком</th>
            <td><?= $model->two_room_first_zone ?></td>
            <td><?= $model->two_room_second_zone ?></td>
            <td><?= $model->two_room_third_zone ?></td>
            <td><?= $model->two_room_fourth_zone ?></td>
            <td><?= $model->two_room_fifth_zone ?></td>
        </tr>
        <tr>
            <th scope="row">3ком</th>
            <td><?= $model->three_room_first_zone ?></td>
            <td><?= $model->three_room_second_zone ?></td>
            <td><?= $model->three_room_third_zone ?></td>
            <td><?= $model->three_room_fourth_zone ?></td>
            <td><?= $model->three_room_fifth_zone ?></td>
        </tr>
        <tr>
            <th scope="row">4ком</th>
            <td><?= $model->four_room_first_zone ?></td>
            <td><?= $model->four_room_second_zone ?></td>
            <td><?= $model->four_room_third_zone ?></td>
            <td><?= $model->four_room_fourth_zone ?></td>
            <td><?= $model->four_room_fifth_zone ?></td>
        </tr>
        <tr>
            <th scope="row">Дом/коттедж</th>
            <td><?= $model->house_first_zone ?></td>
            <td><?= $model->house_second_zone ?></td>
            <td><?= $model->house_third_zone ?></td>
            <td><?= $model->house_fourth_zone ?></td>
            <td><?= $model->house_fifth_zone ?></td>
        </tr>
        </tbody>
    </table>

</div>
