<?php

use app\models\Administrations;
use app\models\Agency;
use app\models\Constant;
use app\models\Photographer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Matrix */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matrix-form">
    <div class="container">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">

            <?php
            $role = Yii::$app->user->identity->getRole();
            if ($role == Constant::ROLE_ADMINISTRATION) {
                $administrator_id = Administrations::findOne(['identity' => Yii::$app->user->id]);

                $agency_list = ArrayHelper::map(Agency::findAll(['administration_id' => $administrator_id->id]), 'id', 'name');
                $photographer_list = ArrayHelper::map(Photographer::findAll(['administration_id' => $administrator_id->id]), 'id', 'fio');
            } else {
                $agency_list = Agency::ddl();
                $photographer_list = Photographer::ddl();
            }
            ?>
            <div class="col-md-8">
                <?= $form->field($model, 'photographer_id')->dropDownList($photographer_list,
                    ['prompt' => 'Выберите фотографа']) ?>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'agency_id')->dropDownList($agency_list,
                    ['prompt' => 'Выберите агента']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'one_room_first_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'two_room_first_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'three_room_first_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'four_room_first_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'house_first_zone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'one_room_second_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'two_room_second_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'three_room_second_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'four_room_second_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'house_second_zone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'one_room_third_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'two_room_third_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'three_room_third_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'four_room_third_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'house_third_zone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'one_room_fourth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'two_room_fourth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'three_room_fourth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'four_room_fourth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'house_fourth_zone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'one_room_fifth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'two_room_fifth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'three_room_fifth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'four_room_fifth_zone')->textInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'house_fifth_zone')->textInput() ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
