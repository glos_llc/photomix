<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matrix */

$this->title = 'Обновить матрицу: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Матрицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="matrix-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
