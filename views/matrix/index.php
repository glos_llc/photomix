<?php

use app\models\{Administrations, Agency, Photographer};
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MatrixSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Матрицы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-index">

    <p>
        <?= Html::a('Создать матрицу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'photographer_id',
                'filter' => $searchModel->getPhotographs(),
                'value' => function ($model) {
                    if (empty(Photographer::findOne(['id' => $model->photographer_id])->fio)) {
                        return 'Не задан';
                    } else {
                        return Photographer::findOne(['id' => $model->photographer_id])->fio;
                    }
                }
            ],
            [
                'attribute' => 'agency_id',
                'filter' => $searchModel->getAgencys(),
                'value' => function ($model) {
                    if (empty(Agency::findOne(['id' => $model->agency_id])->name)) {
                        return 'Не задан';
                    } else {
                        return Agency::findOne(['id' => $model->agency_id])->name;
                    }
                }
            ],
            [
                'attribute' => 'administration',
                'value' => function ($model) {
                    if (is_null($model->photographer_id)){
                        return Administrations::find()->joinWith('agency')->where(['real_estate_agency.id' =>$model->agency_id])->one()->name;
                    }else{
                        return Administrations::find()->joinWith('photographer')->where(['photographer.id' =>$model->photographer_id])->one()->name;
                    }
                },
                'filter' => false,
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter' => false
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
