<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PhotographerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фотографы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photographer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить фотографа', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'entity.login',
            'fio',
            'phone_number',
            'address',
            [
                'attribute' => 'administration_id',
                'value' => 'administrator.name',
            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
