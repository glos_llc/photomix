<?php

use app\models\Administrations;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Photographer */

$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Фотографы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="photographer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'entity.login',
            'fio',
            'phone_number',
            'address',
            [
                'attribute' => 'administration_id',
                'value' => Administrations::findOne(['id' => $model->administration_id])->name,
            ],
            'created',
            'updated',
        ],
    ]) ?>

</div>
