<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Photographer */

$this->title = 'Редактировать фотографа: ' . $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Фотографы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать ';
?>
<div class="photographer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'identity')) ?>

</div>
