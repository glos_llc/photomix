<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Photographer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photographer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($identity, 'login')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord):?>
        <?= $form->field($identity, 'password')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($identity, 'checkPassword')->passwordInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
