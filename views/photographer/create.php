<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Photographer */

$this->title = 'Добавить фотографа';
$this->params['breadcrumbs'][] = ['label' => 'Фотографы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photographer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'identity')) ?>

</div>
