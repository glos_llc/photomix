<?php
use yii\bootstrap\NavBar;
use app\models\Constant;
use yii\bootstrap\Nav;
use yii\helpers\Html;
?>

<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);?>

<?php
$navItems = [];


if (!Yii::$app->user->isGuest) {
    $role = Yii::$app->user->identity->getRole();
    if (Yii::$app->user->can(Constant::ROLE_ADMIN) || ($role == Constant::ROLE_ADMINISTRATION)) {
        $navItems = [
            ['label' => 'Администраторы', 'url' => ['/administrations/index']],
            ['label' => 'Матрица', 'url' => ['/matrix/index']],
            ['label' => 'Пользователи', 'url' => ['/user/index']],
            ['label' => 'Агенства недвижимости', 'url' => ['/agency/index']],
            ['label' => 'Фотографы', 'url' => ['/photographer/index']],
            ['label' => 'Заявки', 'url' => ['/order/index']],
            ['label' => 'Расписание', 'url' => ['/order/schedule']],
            ['label' => 'Карта',
                'items' => [
                    ['label' => 'Карта с заявками', 'url' => ['/map/orders', 'ready' => true]],
                    ['label' => 'Определение зональности ОН', 'url' => ['/map/agency', 'ready' => false]],
                ]
            ],
        ];
    }

    if ($role == Constant::ROLE_ADMINISTRATION) {
        unset($navItems[0]);
    }

    if ($role == Constant::ROLE_PHOTO_MODERATOR) {
        $navItems = [
            ['label' => 'Заявки',
                'items' => [
                    ['label' => 'Готовые', 'url' => ['order/index', 'ready' => true]],
                    ['label' => 'В работе', 'url' => ['order/index', 'ready' => false]],
                ]
            ],
        ];
    }
    if ($role == Constant::ROLE_PHOTOGRAPHER) {
        $navItems = [
            ['label' => 'Заявки', 'url' => ['/order/index']],
            ['label' => 'Расписание', 'url' => ['/order/schedule']],

        ];
    }

    if ($role == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER) {
        $navItems = [
            ['label' => 'Заявки',
                'items' => [
                    ['label' => 'Готовые', 'url' => ['order/index', 'ready' => true]],
                    ['label' => 'В работе', 'url' => ['order/index', 'ready' => false]],
                ]
            ],
            ['label' => 'Фотографы', 'url' => ['/photographer/index']],
            ['label' => 'Расписание', 'url' => ['/order/schedule']],
            ['label' => 'Карта', 'url' => ['/map/orders']],
        ];
    }

    if ($role == Constant::ROLE_REAL_ESTATE_AGENCY) {
        $navItems = [
            ['label' => 'Определение зональности ОН', 'url' => ['/map/agency']],
            ['label' => 'Заявки',
                'items' => [
                    ['label' => 'Готовые', 'url' => ['order/index', 'ready' => true]],
                    ['label' => 'В работе', 'url' => ['order/index', 'ready' => false]],
                ]
            ],
        ];
    }

}
?>

<?php
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => array_merge(
        $navItems,
        [Yii::$app->user->isGuest ? (
        ['label' => 'Вход', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->login . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )]
    ),
]);
NavBar::end();
?>