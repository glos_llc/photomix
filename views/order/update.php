<?php

use yii\helpers\Html;
use app\models\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$url = !in_array(Yii::$app->user->identity->getRole(), [Constant::ROLE_REAL_ESTATE_AGENCY, Constant::ROLE_ADMIN_OF_PHOTOGRAPHER]) ? ['index'] : ['index', 'ready' => true];
$this->title = 'Редактировать заявку №: ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "№$model->number", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать ';
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('forms/_form', [
        'model' => $model,
    ]) ?>

</div>
