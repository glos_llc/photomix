<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\OrderImage;
use app\models\Constant;

?>

<?php $form = ActiveForm::begin([
    'id' => 'order-photos-form',
    'action' => Url::to(['order/download-photos', 'id' => $model->id]),
    'method' => 'POST',
    'options' => ['enctype' => 'multipart/form-data']
]) ?>

<p>
    <?= Html::checkbox('select-all', false, ['style' => ['height' => '20px', 'width' => '20px']]) ?>
    <?= Html::label('Выбрать все', 'select-all') ?>
</p>
<?php foreach ($model->images as $image): ?>
    <div>
        <p>
            <?= Html::checkbox('images[' . $image->image . ']', false, ['style' => ['height' => '20px', 'width' => '20px']]) ?>
            <?= $image->isImage() ?
                Html::a(Html::img($image->getImagePath(),
                ['style' => ['width' => '300px']]) , $image->getImagePath() ,
                ['data-lightbox' => 'images', 'data-title' => '<a class="delete-image-link">Пометить на удаление</a>']) :
                $image->image
            ?>
        </p>
    </div>
    <div class="clearfix"></div>
<?php endforeach; ?>
<p>
    <?= Html::submitButton('Скачать фото', ['class' => 'btn btn-success', 'disabled' => true]) ?>
    <?php if (in_array(Yii::$app->user->identity->getRole(), [Constant::ROLE_ADMIN, Constant::ROLE_PHOTO_MODERATOR, Constant::ROLE_PHOTOGRAPHER])): ?>
        <?= Html::submitButton('Удалить фото', [
                'class' => 'btn btn-danger',
            'value' => 'delete',
            'name' => 'delete',
            'disabled' => true,
            'data' => [
                'confirm' => 'Вы уверены?',
                'method' => 'post',
            ],
        ]) ?>
    <?php endif; ?>
</p>
<?php ActiveForm::end() ?>

<?php
$js = <<<JS

 lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'albumLabel': "Фото %1 из %2",
      'positionFromTop': 200
    });
    
let selectAll = $('input[type="checkbox"][name="select-all"]');
let checkboxes = $('input[type="checkbox"][name!="select-all"]');
let submitButtons =  $('button[type="submit"]', '#order-photos-form');

//Выставление чекбоксов
selectAll.on('change', function(event) {
    let select = $(this);
      if (select.prop('checked')) {
          submitButtons.prop('disabled', false);
          checkboxes.prop('checked', true);
      }else {
           submitButtons.prop('disabled', true);
           checkboxes.prop('checked', false);
      }
  });

checkboxes.on('change', function(event) {
    let select = $(this);
    
    if ($('input[type="checkbox"][name!="select-all"]:checked').length == 0) {
        submitButtons.prop('disabled', true);
    }else {
      submitButtons.prop('disabled', false);  
    }
      if (!select.prop('checked')) {
          selectAll.prop('checked', false);
      }else if($('input[type="checkbox"][name!="select-all"]').not(':checked').length == 0) {
          selectAll.prop('checked', true);
      }
  });

//Выбор чекбокса при клике на "Пометить на удаление"
$(document).on('click', '.delete-image-link', function (e) {
     checkboxes.eq(lightbox.currentImageIndex).prop('checked', true).trigger('change');
     
 });

JS;


$this->registerJs($js);
?>
