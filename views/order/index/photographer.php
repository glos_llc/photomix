<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\models\Constant;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
$ready = Yii::$app->request->get('ready');
$url = ($ready !== null) ? Url::to(['order/index', 'ready' => $ready]) : Url::to(['order/index']);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => $url]); ?>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_from')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_to')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?=Html::submitButton('Фильтр', ['class' => 'btn btn-success', 'style' => ['margin-top' => '22px']])?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?=$this->context->renderPartial('@app/views/order/index/parts/color-legend')?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'number',
                'contentOptions' => function ($model) {
                    return ['class' => $model->getStatusColorClass(true)];
                },
            ],
            [
                'attribute' => 'status',
                'filter' => Constant::orderStatusesDDLByRole(Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, true),
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            [
                'attribute' => 'execution_date_from',
                'label' => 'Дата исполнения',
                'value' => function ($model) {
                    return $model->execution_date_from . ' - ' . $model->execution_date_to;
                }
            ],
            'address',
            [
                    'attribute' => 'agency.name',
                'label' => 'АН',
            ],
            //'description:text',
            'description_admin:text',
            'phone_number',
            'photographer_comment',
            'price:currency',


            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>


</div>
