<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Constant;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

$template = '{view} {update}  {back-to-work} {delete} {download-photos}';
$ready = Yii::$app->request->get('ready');
$url = ($ready !== null) ? Url::to(['order/index', 'ready' => $ready]) : Url::to(['order/index']);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => $url]); ?>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_from')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_to')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?=Html::submitButton('Фильтр', ['class' => 'btn btn-success', 'style' => ['margin-top' => '22px']])?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?=$this->context->renderPartial('@app/views/order/index/parts/color-legend')?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'number',
                'contentOptions' => function ($model) {
                    return ['class' => $model->getStatusColorClass(true)];
                },
            ],
            [
                    'attribute' => 'created_at',
                'label' => 'Дата создания',
                'value' => function($model) {
                    return date('d.m.Y', $model->created_at);
                }

            ],
            'address',
            [
                'attribute' => 'status',
                'filter' => Constant::orderStatuses(true),
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            'client_name',
            'phone_number',
            'realtor',
            [
                    'attribute' => 'planing_contact_date',
                'value' => function($model) {
                    return $model->planing_contact_date ?  date('d.m.Y', strtotime($model->planing_contact_date)) : null;
                }
            ],
            [
                'label' => 'Дата недозвона',
                'value' => function($model) {
                    return $model->changing_status_date ?  date('d.m.Y', strtotime($model->changing_status_date)) : null;
                }
            ],
            [
                'attribute' => 'execution_date_from',
                'label' => 'Дата исполнения',
                'value' => function ($model) {
                    return $model->execution_date_from . ' - ' . $model->execution_date_to;
                }
            ],
            'downloaded:boolean',
            'price_agency',

            ['class' => 'yii\grid\ActionColumn', 'template' => $template,
                'buttons' => [
                    'back-to-work' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-flag"></span>',
                            ['order/back-to-work', 'id' => $model->id],
                            ['title' => 'Вернуть в работу']);

                    },
                    'download-photos' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>',
                            ['order/download-all-photos', 'id' => $model->id],
                            ['title' => 'Скачать фото', 'class' => 'btn btn-primary']);

                    },
                ],
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return $model->status == Constant::ORDER_STATUS_NEW;
                    },
                    'delete' => function ($model, $key, $index) {
                        return $model->status == Constant::ORDER_STATUS_NEW;
                    },
                    'back-to-work' => function ($model, $key, $index) {
                        return $model->isReady();
                    },
                    'download-photos' => function ($model, $key, $index) {
                        return in_array($model->status, [Constant::ORDER_STATUS_DOWNLOADED, Constant::ORDER_STATUS_FAILED]);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
