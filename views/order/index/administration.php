<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\Agency;
use app\models\Photographer;
use kartik\select2\Select2;
use app\models\Constant;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/change-order-price.js', ['depends' => \app\assets\AppAsset::class]);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <div class="row">
        <div class="col-md-2"><?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?></div>
        <div class="col-md-2 col-md-offset-8"><?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => false,
                'showColumnSelector' => false,
                'filename' => 'Orders ' . date('Y_m_d'),
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_CSV => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_PDF => false,
                    ExportMenu::FORMAT_EXCEL_X => [
                        'label' => '<span class="glyphicon glyphicon-list-alt"></span> Сохранить в Excel',
                        'iconOptions' => ['class' => 'hidden'],
                        'linkOptions' => ['class' => 'btn btn-info'],
                        'options' => ['style' => 'list-style-type:none; display:inline-block'],
                    ],
                ],
                'asDropdown' => false,
                'showFooter' => true,
                'showHeader' => true,
                'showPageSummary' => true,
                'caption' => 'ФИО',
                /*'contentBefore' => [
                    'product_id' => [
                        'value' => "{$model->productCatalog->name} (id - $model->client_id)"
                    ]
                ],*/
                //'captionOptions' => ['class' => 'export_table_caption'],
                //'showPageSummary' => true,
                'columns' => [
                    'number',
                    'agency.name',
                    'client_name',
                    'realtor',
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return $model->getStatus();
                        }
                    ],
                    'roomQntType',
                    'zone',
                    'address',
                    'description',
                    'phone_number',
                    'period:html',
                    'planing_contact_date',
                    'changing_status_date',
                    'price',
                    'price_agency',
                    'photographer.fio',
                    'photographer_comment',
                    'downloaded:boolean',
                    'created',
                ]
            ]); ?></div>
    </div>
    </p>
    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['order/index'])]); ?>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_from')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'date_to')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy H:ii',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('Фильтр', ['class' => 'btn btn-success', 'style' => ['margin-top' => '22px']]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <?=$this->context->renderPartial('@app/views/order/index/parts/color-legend')?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'number',
                'contentOptions' => function ($model) {
                    return ['class' => $model->getStatusColorClass(true)];
                },
            ],
            [
                'attribute' => 'agency_id',
                'label' => 'АН',
                'filter' => Select2::widget([
                    'name' => 'OrderSearch[agency_id]',
                    'model' => $searchModel,
                    'data' => Agency::ddl(),
                    'value' => $searchModel->agency_id,
                    'options' => ['multiple' => false, 'placeholder' => 'Выберите АН'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function ($model) {
                    return $model->agency->name;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Constant::orderStatuses(true),
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            [
                'attribute' => 'photographer_id',
                'filter' => Select2::widget([
                    'name' => 'OrderSearch[photographer_id]',
                    'model' => $searchModel,
                    'data' => Photographer::ddlFilter(),
                    'value' => $searchModel->photographer_id,
                    'options' => ['multiple' => false, 'prompt' => 'Выберите фотографа'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function ($model) {
                    return $model->photographer ? $model->photographer->fio : null;
                }
            ],
            'address',
            [
                    'attribute' => 'price',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::input('number', 'price', $model->price, ['class' => 'change-price-input', 'data' => [
                            'id' => $model->id,
                            'agency' => 0
                        ]]) . ' &#8381;';
                }
            ],
            [
                'attribute' => 'price_agency',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::input('number', 'price', $model->price_agency, ['class' => 'change-price-input', 'data' => [
                            'id' => $model->id,
                            'agency' => 1
                        ]]) . ' &#8381;';
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}  {delete}{download-photos}',
                'buttons' => [
                    'download-photos' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>',
                            ['order/download-all-photos', 'id' => $model->id],
                            ['title' => 'Скачать фото', 'class' => 'btn btn-primary']);

                    },
                ],
                'visibleButtons' => [
                    'download-photos' => function ($model, $key, $index) {
                        return $model->status === Constant::ORDER_STATUS_DOWNLOADED;
                    }
                ]
            ],
        ],
    ]); ?>


</div>
