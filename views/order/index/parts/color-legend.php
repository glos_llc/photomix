<?php
?>

<p>
<div class="row">
    <div class="col-md-2">
        <div class="status-color-icon icon-color-blueStretchyIcon"><b>Новая заявка</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-yellowStretchyIcon"><b>Недозвон 1,2</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-violetStretchyIcon"><b>Предварительный созвон</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-lightGreen"><b>На исполнении</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-green"><b>На модерации</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-darkGreen"><b>Фото загружены</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-orange"><b>Фото скачаны</b></div>
    </div>
    <div class="col-md-2">
        <div class="status-color-icon icon-color-redStretchyIcon"><b>Отказ клиента</b></div>
    </div>
</div>
</p>
