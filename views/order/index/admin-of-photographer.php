<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Agency;
use app\models\Photographer;
use kartik\select2\Select2;
use app\models\Constant;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

$template = '{view} {update-description} {change-status}';
$ready = Yii::$app->request->get('ready', 0);
$this->registerJsFile('/js/ajax-update.js', ['depends' => \app\assets\AppAsset::class]);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=$this->context->renderPartial('@app/views/order/index/parts/color-legend')?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'number',
                'contentOptions' => function ($model) {
                    return ['class' => $model->getStatusColorClass(true)];
                },
            ],
            [
                'attribute' => 'agency_id',
                'label' => 'АН',
                'filter' => Select2::widget([
                    'name' => 'OrderSearch[agency_id]',
                    'model' => $searchModel,
                    'data' => Agency::ddl(),
                    'value' => $searchModel->agency_id,
                    'options' => ['multiple' => false, 'placeholder' => 'Выберите АН'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'value' => function ($model) {
                    return $model->agency->name;
                }
            ],
            'address',
            'phone_number',
            'client_name',
            'realtor',
            [
                    'attribute' => 'room_qnt_type',
                'filter' => Constant::qntRoomTypes(true),
                'value' => function($model) {
                    return $model->getRoomQntType();
                }
            ],
            [
                'attribute' => 'status',
                'filter' => $ready ? Constant::readyOrderStatuses(true) : Constant::notReadyOrderStatuses(true),
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            'planing_contact_date',
            'description:raw',
            'description_admin:raw',

            ['class' => 'yii\grid\ActionColumn', 'template' => $template,
                'buttons' => [
                    'change-status' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-flag"></span>',
                            ['order/change-status', 'id' => $model->id],
                            ['title' => 'Изменить статус']);

                    },
                    'update-description' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>','#', [
                            'class' => 'update-description',
                            'value' => Url::to(['order/update-description', 'id' => $model->id]),
                            'title' => 'Редактировать примечание'
                        ]);

                    },
                ],
                'visibleButtons' => [
                        'change-status' => function ($model, $key, $index) {
                            return !$model->isReady();
                        }
                ]
            ],

        ],
    ]); ?>


</div>
<?php
Modal::begin([
    'header' => '<h4>Редактировать примечание</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

