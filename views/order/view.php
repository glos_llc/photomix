<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Constant;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$url = in_array(Yii::$app->user->identity->getRole(), [Constant::ROLE_REAL_ESTATE_AGENCY, Constant::ROLE_ADMIN_OF_PHOTOGRAPHER]) ?
    ['index', 'ready' => $model->isReady()] :
    ['index'];
$this->title = 'Заявка № ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$attributes = [];
if (in_array(YIi::$app->user->identity->getRole(), [Constant::ROLE_ADMIN, Constant::ROLE_ADMIN_OF_PHOTOGRAPHER, Constant::ROLE_ADMINISTRATION])) {
    $attributes = [
        'id',
        'number',
        [
            'attribute' => 'agency.name',
            'label' => 'АН'
        ],
        'realtor',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model->getStatus();
            }
        ],
        'roomQntType',
        'zone',
        'address',
        'description:raw',
        'description_admin:raw',
        'client_name',
        'phone_number',
        [
            'attribute' => 'call_history',
            'visible' => (Yii::$app->user->identity->getRole() == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER ||
                Yii::$app->user->identity->getRole() == Constant::ROLE_ADMIN)
        ],
        [
            'attribute' => 'execution_date_from',
            'label' => 'Дата исполнения',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->execution_date_from . ' &mdash; ' . $model->execution_date_to;
            }
        ],
        [
            'attribute' => 'planing_contact_date',
            'value' => function ($model) {
                return $model->planing_contact_date;
            }
        ],
        [
            'attribute' => 'changing_status_date',
            'value' => function ($model) {
                return $model->changing_status_date;
            }
        ],
        'price:currency',
        [
            'attribute' => 'price_agency',
            'format' => 'currency',
            'visible' => Yii::$app->user->can('admin')
        ],
        'photographer.fio',
        'photographer_comment:raw',
        'downloaded:boolean',
        'created',
        'updated',
        [
            'label' => 'Фото',
            'format' => 'raw',
            'visible' => $model->images,
            'value' => function ($model) {
                return $this->context->renderPartial('parts/images', compact('model'));
            }
        ]
    ];
}

if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY) {
    $attributes = [
        'number',
        'address',
        'roomQntType',
        'zone',
        'client_name',
        'phone_number',
        'realtor',
        'description',
        'price_agency',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model->getStatus();
            }
        ],
        [
            'attribute' => 'photographer_comment',
            'format' => 'raw',
            'visible' => $model->status === Constant::ORDER_STATUS_DOWNLOADED
        ],
        [
            'label' => 'Фото',
            'format' => 'raw',
            'visible' => in_array($model->status, [Constant::ORDER_STATUS_DOWNLOADED, Constant::ORDER_STATUS_FAILED]) && $model->images,
            'value' => function ($model) {
                return $this->context->renderPartial('parts/images', compact('model'));
            }
        ]
    ];
}
if (Yii::$app->user->identity->getRole() == Constant::ROLE_PHOTOGRAPHER) {
    $attributes = [
        'number',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model->getStatus();
            }
        ],
        'address',
        'description_admin',
        [
            'attribute' => 'execution_date_from',
            'label' => 'Дата исполнения',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->execution_date_from . ' &mdash; ' . $model->execution_date_to;
            }
        ],
        'phone_number',
        'price:currency',
        //'description_admin:raw',
        'photographer_comment:raw',
        [
            'label' => 'Фото',
            'format' => 'raw',
            'visible' => $model->images,
            'value' => function ($model) {
                return $this->context->renderPartial('parts/images', compact('model'));
            }
        ]
    ];
}

if (Yii::$app->user->identity->getRole() == Constant::ROLE_PHOTO_MODERATOR) {
    $attributes = [
        'number',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model->getStatus();
            }
        ],
        'address',
        'photographer_comment:raw',
        'downloaded:boolean',
        [
            'label' => 'Фото',
            'format' => 'raw',
            'visible' => $model->images,
            'value' => function ($model) {
                return $this->context->renderPartial('parts/images', compact('model'));
            }
        ]
    ];
}

?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can(Constant::ROLE_ADMIN)): ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>

    <?php if (Yii::$app->user->identity->getRole() == Constant::ROLE_PHOTOGRAPHER): ?>
        <p>
            <?= Html::a('Добавить фото/комментарий', ['order/upload-photos', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
            <?= ($model->status != Constant::ORDER_STATUS_FAILED) ? Html::a('Ложная заявка', ['order/fail', 'id' => $model->id], ['class' => 'btn btn-danger']) : null ?>
        </p>

    <?php endif; ?>

    <?php if (Yii::$app->user->identity->getRole() == Constant::ROLE_PHOTO_MODERATOR): ?>
        <p>
            <?= Html::a('Отправить АН', ['order/send-to-agency', 'id' => $model->id], ['class' => 'btn btn-info', 'disabled' => empty($model->images)]) ?>
            <?= Html::a('Добавить фото / Редактировать', ['order/upload-photos', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>

    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

    <?php if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY): ?>

        <div class="pull-right">
            <?= Html::a('Создать следующую заявку ', ['order/create'], ['class' => 'btn btn-success']) ?>
        </div>

    <?php endif; ?>


</div>
