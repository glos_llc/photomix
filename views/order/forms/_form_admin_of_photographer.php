<?php

use app\models\Constant;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактировать примечание' . $model->number;
?>

<?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

<?= $form->field($model, 'description_admin')->textarea(['row' => 6]) ?>

<?php
if (Yii::$app->user->identity->getRole() == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER ||
    Yii::$app->user->identity->getRole() == Constant::ROLE_ADMIN) {
    echo $form->field($model, 'call_history')->textarea(['row' => 6]);
}
?>

    <p>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </p>

<?php ActiveForm::end() ?>