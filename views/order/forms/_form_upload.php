<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Загрузить фото к заявке №' . $model->number;
?>

<h1><?=$this->title?></h1>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($photoForm, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Изображения') ?>
    <?= $form->field($model, 'photographer_comment')->textarea(['row' => 6]) ?>

    <p>
        <?=Html::submitButton('Загрузить фото', ['class' => 'btn btn-success'])?>
    </p>

<?php ActiveForm::end() ?>