<?php

use app\models\Administrations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Constant;
use app\models\Agency;
use app\models\Photographer;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-4"><?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-4"><?= $form->field($model, 'apartment_number')->textInput(['maxlength' => true]) ?></div>
    </div>


    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'room_qnt_type')->dropDownList(Constant::qntRoomTypes(true), ['prompt' => 'Выберите комнатность']) ?></div>

        <div class="col-md-4"><?= $form->field($model, 'zone')->dropDownList(Constant::getZoneTypes(true), ['prompt' => 'Выберите зону']) ?></div>
        <?php if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY) { ?>
            <div class="col-md-4"><?= $form->field($model, 'price_agency')->textInput(['type' => 'number', 'disabled' => true]) ?></div>
        <?php } else { ?>
            <div class="col-md-4"><?= $form->field($model, 'price_agency')->textInput(['type' => 'number']) ?></div>
        <?php } ?>
    </div>

    <?php
    if ($model->isNewRecord) {
        $model->checker = 0;
        ?>

        <?= $form->field($model, 'checker')->radioList([0 => 'адресу', 1 => 'координатам'], ['checked' => 0])->label('Определять по'); ?>

        <?= $form->field($model, 'coordinates')->textInput()->label('Координаты, формат должен быть: 55.047404, 82.906164') ?>

        <?php
    }
    ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'realtor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php if (Yii::$app->user->can(Constant::ROLE_ADMIN) || Yii::$app->user->can(Constant::ROLE_ADMINISTRATION)): ?>

        <?php
        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            $administrator_id = Administrations::findOne(['identity' => Yii::$app->user->id]);

            $agency_list = ArrayHelper::map(Agency::findAll(['administration_id' => $administrator_id->id]), 'id', 'name');
            $photographer_list = ArrayHelper::map(Photographer::findAll(['administration_id' => $administrator_id->id]), 'id', 'fio');
        } else {
            $agency_list = Agency::ddl();
            $photographer_list = Photographer::ddl();
        }
        ?>

        <?= $form->field($model, 'agency_id')->widget('\kartik\select2\Select2', [
            // 'name' => 'newDetailId',
            'data' => $agency_list,
            'options' => ['multiple' => false, 'placeholder' => 'Выберите агенство'],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <?= $form->field($model, 'execution_date_from')->widget(DateTimePicker::class, [
            // 'name' => 'check_issue_date',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Выберите дату'],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy H:ii',
                'todayHighlight' => true,
            ]
        ]); ?>

        <?= $form->field($model, 'execution_date_to')->widget(DateTimePicker::class, [
            // 'name' => 'check_issue_date',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Выберите дату'],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy H:ii',
                'todayHighlight' => true
            ]
        ]); ?>

        <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>

        <?= $form->field($model, 'photographer_id')->widget('\kartik\select2\Select2', [
            'data' => $photographer_list,
            'options' => ['multiple' => false, 'placeholder' => 'Выберите фотографа'],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
if (Yii::$app->user->identity->getRole() == Constant::ROLE_REAL_ESTATE_AGENCY) {

    $js = <<<JS
    $(document).on('change', '#order-room_qnt_type', function(e) {
        calculate(e);
    });

    $(document).on('change', '#order-zone', function(e) {
            calculate(e);
        });

    function calculate(e) {
         e.preventDefault();
         let selectZone = document.getElementById("order-zone");
         let zone = selectZone.options[selectZone.selectedIndex].value;
         
         let selectRoom_qnt_type = document.getElementById("order-room_qnt_type");
         let room_qnt_type = selectRoom_qnt_type.options[selectRoom_qnt_type.selectedIndex].value;
         
         if (zone === '') {
              return false;
         }
         
         if (room_qnt_type === '' ) {
              return false;
         }

         $.ajax({
             url: '/order/calculate-agency-price',
             type: 'POST',
             data: {
                 "zone": zone,
                 "room_qnt_type": room_qnt_type,
             },
             success: function(res){
                            if (res == false){
                                  alert('Произошла ошибка');
                            } else{
                                document.getElementById('order-price_agency').value = res
                            }
                    },
             error: function(){
                        alert('Заполните все поля');
             }
         });
    }
JS;

    $this->registerJs($js);
}
?>
