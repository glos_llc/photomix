<?php

use app\models\Administrations;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\models\Photographer;
use app\models\Constant;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$this->title = "Изменить статус заявки №$model->number";
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=30071976-3b4b-46b5-9531-5d5594e49e96&width=640&height=480');

if ($orders) {
    $mapJs = "ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.010317, 82.950156],
            zoom: 11,
        }, {
            searchControlProvider: 'yandex#search'
        });

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style=\"color: #FFFFFF; font-weight: bold;\">$[properties.iconContent]</div>'
        ); ";

    foreach ($orders as $photographerId => $orderArray) {
        foreach ($orderArray['models'] as $order) {
            $iconContent = date('d.m', strtotime($order->execution_date_from));
            $mapJs .= "
        
        myPlacemarkWithContent = new ymaps.Placemark([{$order->getCoordinatesForMap()}], {
            balloonContentHeader: '" . Html::a('Заявка №' . $order->number, ['order/view', 'id' => $order->id], ['target' => '_blank']) . "', 
            balloonContent: '<p>$order->address<br>$order->shortPeriod</p>',
            iconContent : '$iconContent',
        }, {
            preset: '{$orderArray['color']}'
        });
        
        myMap.geoObjects.add(myPlacemarkWithContent);
        ";
        }
    }

    $mapJs .= " }); ";
    $this->registerJs($mapJs);
}
?>

<div class="status_form">

    <?php // $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Constant::orderStatuses(true)) ?>

    <div id="contact-date-block">
        <?= $form->field($model, 'planing_contact_date')->widget(DateTimePicker::class, [
            // 'name' => 'check_issue_date',
            // 'value' => date('d-M-Y', strtotime('+2 days')),
            'options' => ['placeholder' => 'Выберите дату'],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy H:ii',
                'todayHighlight' => true,
            ]
        ]); ?>
    </div>


    <div id="start-execution-block">

        <?php
        $role = Yii::$app->user->identity->getRole();
        if ($role == Constant::ROLE_ADMINISTRATION) {
            $administrator_id = Administrations::findOne(['identity' => Yii::$app->user->id]);
            $photographer_list = ArrayHelper::map(Photographer::findAll(['administration_id' => $administrator_id->id]), 'id', 'fio');
        } else if ($role == Constant::ROLE_ADMIN_OF_PHOTOGRAPHER) {

            $user_id = User::findOne(['identity' => Yii::$app->user->id]);

            $administrator_id = Administrations::findOne(['id' => $user_id->administration_id]);
            $photographer_list = ArrayHelper::map(Photographer::findAll(['administration_id' => $administrator_id->id]), 'id', 'fio');
        } else {
            $photographer_list = Photographer::ddl();
        }
        ?>

        <?= $form->field($model, 'photographer_id')->widget('\kartik\select2\Select2', [
            'data' => $photographer_list,
            'options' => ['multiple' => false, 'placeholder' => 'Выберите фотографа'],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'execution_date_from')->widget(DateTimePicker::class, [
                    // 'name' => 'check_issue_date',
                    // 'value' => date('d-M-Y', strtotime('+2 days')),
                    'options' => ['placeholder' => 'Выберите дату'],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy H:ii',
                        'todayHighlight' => true,
                    ]
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'execution_date_to')->widget(DateTimePicker::class, [
                    // 'name' => 'check_issue_date',
                    // 'value' => date('d-M-Y', strtotime('+2 days')),
                    'options' => ['placeholder' => 'Выберите дату'],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy H:ii',
                        'todayHighlight' => true
                    ]
                ]); ?>
            </div>
        </div>
        <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>

        <?php if ($orders): ?>
            <fieldset style="margin: 10px 0">
                <legend>Назначение фотографов</legend>
                <div class="flex-box-orders">
                    <?php foreach ($orders as $ordersData): ?>
                        <div class="">
                            <p>
                                <span class="<?= Constant::YMAP_ICON_PRESETS_COLOR_CORRELATION[$ordersData['color']] ?>">
                                    <?= $ordersData['models'][0]->photographer->fio; ?>
                                </span>
                            </p>
                            <p>
                                <?php foreach ($ordersData['models'] as $order): ?>
                            <p>
                                    <span>
                                        Заявка №<?= Html::a($order->number, ['order/view', 'id' => $order->id, ['target' => '_target']]) ?>
                                    </span>
                                <br>
                                <span><?= $order->period ?></span><br>
                            </p>
                            <?php endforeach; ?>
                            </p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </fieldset>

            <div id="map" style="width: 100%; height: 500px; padding: 0; margin: 0;"></div>
        <?php endif; ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$js = <<<JS
let ddl = $('#order-status');
  if (ddl.val() == 'status-missed-call-1' || ddl.val() == 'status-missed-call-2' || ddl.val() == 'status-prelim-call') {
      $('#contact-date-block').show();
  }else if (ddl.val() == 'status-executing') {
      $('#start-execution-block').show();
  }
  
  ddl.on('change', function(event) {
      if (ddl.val() == 'status-prelim-call') {
        $('#contact-date-block').slideDown();
        $('#start-execution-block').slideUp();
        }else if(ddl.val() == 'status-executing') {
          $('#contact-date-block').slideUp();
          $('#start-execution-block').slideDown();
        } else {
          $('#contact-date-block').slideUp();
          $('#start-execution-block').slideUp();
        }
  });
  
JS;
$this->registerJs($js);
?>

<?php
$js = <<<JS
    $(document).on('change', '#order-photographer_id', function(e) {
         e.preventDefault();
         let id = $model->id;
         let selectPhotographer_id = document.getElementById("order-photographer_id");
         let photographer_id = selectPhotographer_id.options[selectPhotographer_id.selectedIndex].value;
         
         $.ajax({
             url: '/order/calculate-photograph-price',
             type: 'POST',
             data: {
                 "photographer_id": photographer_id,
                 "id": id,
             },
             success: function(res){
                            if (res == false){
                                  alert('Произошла ошибка');
                            } else{
                                document.getElementById('order-price').value = res
                            }
                    },
             error: function(){
                        alert('Произошла ошибка');
             }
         });
    });
JS;

$this->registerJs($js);
?>
