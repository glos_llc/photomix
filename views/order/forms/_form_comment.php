<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Изменить комментарий к заявке №' . $model->number;
?>

    <h1><?=$this->title?></h1>
<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'photographer_comment')->textarea(['row' => 6]) ?>

    <p>
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    </p>

<?php ActiveForm::end() ?>