<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Ложная заявка';
?>

<h1><?=$this->title?></h1>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($photoForm, 'images[]')->fileInput(['multiple' => true])->label('Добавить файл') ?>
    <?= $form->field($model, 'photographer_comment')->textarea(['row' => 6]) ?>

    <p>
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    </p>

<?php ActiveForm::end() ?>