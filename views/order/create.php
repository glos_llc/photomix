<?php

use yii\helpers\Html;
use app\models\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$url = !in_array(Yii::$app->user->identity->getRole(), [Constant::ROLE_REAL_ESTATE_AGENCY, Constant::ROLE_ADMIN_OF_PHOTOGRAPHER]) ? ['index'] : ['index', 'ready' => false];
$this->title = 'Создать заявку';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('forms/_form', [
        'model' => $model,
    ]) ?>

</div>
