<?php
use yii\helpers\Html;

$this->title = 'Расписание';
?>

<h1><?= $this->title ?></h1>
<div class="row">
    <div class="col-md-10">
        <table class="table table-bordered schedule-table">
            <?php foreach ($modelsByPhotographer as $key => $modelsArray): ?>
                <tr class="row-photographer">
                    <td><span class="schedule-photographer"
                              data-id="<?= $key ?>"><?= $modelsArray[0]->photographer->fio ?></span>
                        (<?= count($modelsArray) ?>)
                    </td>
                    <td></td>
                </tr>
                <?php foreach ($modelsArray as $model): ?>
                        <tr data-photographer="<?= $key ?>">
                            <td><?= $model->period ?></td>
                            <td><?=Html::a("Заявка №$model->number", ['order/view', 'id' => $model->id])?>, <?=$model->address?></td>
                        </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?php
$js = <<<JS
$(document).on('click','.schedule-photographer', function(e) {
       let id = $(this).attr('data-id');
       $('tr[data-photographer="' + id + '"]').fadeToggle();
    });

JS;
$this->registerJs($js);
?>