<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->dropDownList(Constant::userRoles(true)) ?>

    <?= $form->field($identity, 'login')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord):?>
    <?= $form->field($identity, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($identity, 'checkPassword')->passwordInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
