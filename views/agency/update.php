<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agency */

$this->title = 'Редактировать АН: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'АН', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать ';
?>
<div class="agency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'identity')) ?>

</div>
