<?php

use app\models\Administrations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($identity, 'login')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($identity, 'password')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($identity, 'checkPassword')->passwordInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, 'administration_id')->dropDownList(
            ArrayHelper::map(Administrations::find()->all(), 'id', 'name')
            , ['prompt' => 'Выберите администратора', 'required' => true]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
