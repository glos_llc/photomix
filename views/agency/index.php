<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Агенства недвижимости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'entity.login',
            'name',
            [
                'label' => 'Администратор',
                'attribute' => 'administration_id',
                'value' => 'administrations.name',
            ],
            [
                'label' => 'Город',
                'attribute' => 'city',
                'value' => 'administrations.city',
            ],
            'created',
            'updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
