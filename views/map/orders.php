<?php
use yii\helpers\Html;
use app\models\Constant;

$this->title = 'Карта по статусам';
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=30071976-3b4b-46b5-9531-5d5594e49e96&width=640&height=480');

$dataData = array(
    'type' => 'FeatureCollection',
    'features' => [],
);

$mapJs = '';
$isNsk = true;


$mapJs .= " }); ";

//Добавляем код инициализации карты
//Добавляем в конце для возможности изменения зума
$mapJs = "ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.010317, 82.950156],
            zoom: " . ($isNsk ? 11 : 6) . ",
        }, {
            searchControlProvider: 'yandex#search'
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });
        
        myMap.geoObjects.add(objectManager);
        $.ajax({
            url: 'get-json-object-for-map'
        }).done(function(data) {
            console.log(data);
            objectManager.add(data);
        });
        
        
        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style=\"color: #FFFFFF; font-weight: bold;\">$[properties.iconContent]</div>'
        ); " . $mapJs;
$this->registerJs($mapJs);
?>

<h1><?=$this->title?></h1>

<div>
    <?=Html::button(Constant::orderStatuses(true)[Constant::ORDER_STATUS_NEW],
        ['class' => 'btn-color-blueStretchyIcon colored-status'])?>

    <?=Html::button(Constant::orderStatuses(true)[Constant::ORDER_STATUS_PRELIM_CALL],
        ['class' => 'btn-color-violetStretchyIcon colored-status'])?>

    <?=Html::button(Constant::orderStatuses(true)[Constant::ORDER_STATUS_MISSED_CALL_1],
        ['class' => 'btn-color-yellowStretchyIcon colored-status'])?>

    <?=Html::button(Constant::orderStatuses(true)[Constant::ORDER_STATUS_MISSED_CALL_2],
        ['class' => 'btn-color-yellowStretchyIcon colored-status'])?>

</div>

<p>
<div id="map" style="width: 100%; height: 500px; padding: 0; margin: 0;"></div>
</p>
