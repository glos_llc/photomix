<?php
namespace app\components\traits;

trait BooleanAttribute
{
    /**
     * @param string $attrName
     * @return string
     */
    public function getBooleanAttrValue(string $attrName)
    {
        return $this->{$attrName} ? 'Да' : 'Нет';
    }

    static function getYesNo()
    {
        return ['Нет', 'Да'];
    }
}