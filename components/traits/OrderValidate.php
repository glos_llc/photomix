<?php


namespace app\components\traits;


use app\helpers\AddressHelper;
use app\helpers\GeoHelper;

trait OrderValidate
{
    /**
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function checked()
    {
        if ($this->checker == 0) {
            return $this->addressValidate();
        } else {
            return $this->validateCoordinate();
        }
    }

    /**
     * @return bool
     *
     * @throws \yii\web\HttpException
     */
    public function validateCoordinate()
    {
        if (empty($this->coordinates)) {
            $this->addError('coordinates', 'Заполните координаты');
            return false;
        }
        $addressHelper = new AddressHelper();
        $yandexXml = $addressHelper->getAddress($this->coordinates);
        if ($yandexXml) {
            if ($addressHelper->parseAddress($yandexXml, $city, $street, $house)) {
                $this->city = $city;
                $this->street = $street;
                $this->house = $house;
                return true;
            }
        }
        $this->addError('coordinates', 'Не удалось определить адрес по этим координатам');
        return false;
    }

    /**
     * @return bool
     *
     * @throws \yii\web\HttpException
     */
    public function addressValidate()
    {
        if (empty($this->city) || empty($this->street) || empty($this->house)) {
            $this->addError('city', 'Заполните город');
            $this->addError('street', 'Заполните улицу');
            $this->addError('house', 'Заполните дом');
            return false;
        }
        $coordinates = GeoHelper::getCoordinates($this->city, $this->street, $this->house);
        if (!$coordinates) {
            $this->addError('city', 'Неверно указан адрес');
            $this->addError('street', 'Неверно указан адрес');
            $this->addError('house', 'Неверно указан адрес');
            return false;
        }
        $this->coordinates = $coordinates;
        return true;
    }
}