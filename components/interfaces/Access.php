<?php


namespace app\components\interfaces;


interface Access
{
    public function checkAccess($id);
}