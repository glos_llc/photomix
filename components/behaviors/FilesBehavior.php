<?php
namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\imagine\Image;
use app\components\ImageResizer;
use app\components\ImageRotator;

class FilesBehavior extends Behavior
{
    /**
     * @var mixed
     */
    public $filesAttrName;

    /**
     * @var object
     */
    public $filesClassName;

    /**
     * @var string
     */
    public $filesClassAttrName;

    /**
     * @var integer
     */
    public $filesClassMaterialIdAttrName;

    /**
     * @var string
     */
    public $deleteFilesAttrName;

    /**
     * @var string
     */
    public $dir;


    /**
     * @var integer
     */
    public $filesClassMaterialTypeAttrName;

    /**
     * @var integer
     */
    public $materialType;

    /**
     * @var mixed
     */
    public $watermark = false;

    /**
     * @var int
     */
    public $width = null;

    /**
     * @var int
     */
    public $height = null;
    /**
     * @var string
     */
    public $thumbDir;
    /**
     * @var null | int
     */
    public $thumbWidth = null;
    /**
     * @var null | int
     */
    public $thumbHeight = null;
    /**
     * @var null | int
     */
    public $rotation = null;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT=> 'afterInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE=> 'beforeDelete',
        ];
    }

    /**
     * @return ImageResizer
     */
    public function getResizer()
    {
        return new ImageResizer();
    }

    /**
     * @return ImageRotator
     */
    public function getRotator()
    {
        return new ImageRotator();
    }

    /**
     * @return bool
     */
    public function afterInsert() {
        $this->upload();
    }

    /**
     * @return bool
     */
    public function beforeUpdate() {
        $this->delete();
        $this->upload();
    }

    /**
     * @return bool
     */
    public function beforeDelete() {
        $class = $this->filesClassName;
        $models = $class::findAll([$this->filesClassMaterialIdAttrName => $this->owner->id]);

        foreach($models as $model) {
            $this->unlinkFile($this->thumbDir, $model->{$this->filesClassAttrName});
            $this->unlinkFile($this->dir, $model->{$this->filesClassAttrName});
            $model->delete();
        }
    }

    /**
     * @throws ErrorException
     */
    private function upload()
    {
        $filesArray = $this->owner->{$this->filesAttrName};

        if (is_array($filesArray) && !empty($filesArray) && $filesArray[0] instanceof UploadedFile) {
            $files = $filesArray;
        } else {
            $files = UploadedFile::getInstances($this->owner, $this->filesAttrName);
        }

        if (count($files) > 0) {
            foreach ($files as $file) {
                $model = new $this->filesClassName;
                $fileName = time() . '_' . md5(Yii::$app->security->generateRandomString(20)) . '.' . strtolower($file->extension);;
                $model->{$this->filesClassAttrName} = $fileName;
                $model->{$this->filesClassMaterialIdAttrName} = $this->owner->id;
                $model->{$this->filesClassMaterialTypeAttrName} = $this->materialType;

                $model->save();
                !file_exists($this->dir) && mkdir($this->dir, '0777', true);
                $file->saveAs($this->dir . $fileName);

                if ($this->rotation !== null) {
                    $this->rotate($this->dir . $fileName);
                }

                if ($this->width != null || $this->height != null) {
                    $this->resize($this->dir . $fileName);
                }

                if ($this->thumbWidth != null || $this->thumbHeight != null) {
                    $this->createThumbnail($this->dir, $fileName);
                }

                if ($this->watermark !== false) {
                    $this->getWatermarkAdder()->addWatermark($this->dir, $fileName, $file->extension, $this->watermark);
                }
            }
        }
    }

    /**
     * Delete several files through array([id] => [0 or 1])
     */
    private function delete() {
        $class = $this->filesClassName;
        $deleteArray = $this->owner->{$this->deleteFilesAttrName};

        foreach ($deleteArray as $key => $value) {
            if ($value) {
                $fileModel = $class::findOne($key);
                $fileName =  $fileModel->{$this->filesClassAttrName};

                $this->unlinkFile($this->thumbDir, $fileName);
                $this->unlinkFile($this->dir, $fileName);
                $fileModel->delete();
            }
        }
    }

    /**
     * @param $file
     */
    private function resize($file)
    {
        $resizer = $this->getResizer();
        $resizer->setFile($file);

        if ($this->width != null) {
            $resizer->resizeToWidth($this->width);
        }

        if ($this->height != null) {
            $resizer->resizeToHeight($this->height);
        }

        $resizer->save($file);
    }

    /**
     * @param $file
     */
    private function rotate($file)
    {
        $rotator = $this->getRotator();
        $rotator->setFile($file);
        $rotator->rotate($this->rotation);

        $rotator->save($file);
    }

    private function createThumbnail($dir, $fileName)
    {
        !file_exists($this->thumbDir) && mkdir($this->thumbDir);
        Image::thumbnail($dir . $fileName, $this->thumbWidth, $this->thumbHeight)
            ->save(Yii::getAlias($this->thumbDir.$fileName));
    }

    /**
     * @param $dir
     * @param $fileName
     * @return bool
     */
    private function unlinkFile($dir, $fileName)
    {
        if ($fileName != null && file_exists($dir . $fileName)) {
            unlink($dir . $fileName);
        }
    }
}
?>