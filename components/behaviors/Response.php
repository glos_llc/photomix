<?php


namespace app\components\behaviors;


use Yii;
use yii\base\Behavior;

class Response extends Behavior
{
    /**
     * @param $message
     *
     * @return array
     */
    public static function responseError($message)
    {
        Yii::$app->response->statusCode = 400;

        return ['status' => 'error', 'code' => 400, 'error' => $message];
    }

    /**
     * @param $message
     *
     * @return array
     */
    public static function responseNotFound($message)
    {
        Yii::$app->response->statusCode = 404;

        return ['status' => 'error', 'code' => 404, 'error' => $message];
    }

    /**
     * @return array
     */
    public static function responseAccessDenied()
    {
        Yii::$app->response->statusCode = 403;

        return ['status' => 'error', 'code' => 403, 'error' => 'Доступ запрещён.'];
    }

    /**
     * @param            $data
     * @param array|null $static
     *
     * @return array
     */
    public static function succeededWithData($data, array $static = null)
    {
        Yii::$app->response->statusCode = 200;

        if ($static) {
            return ['status' => 'success', 'code' => '200', 'data' => [$data, ['static' => $static]]];
        }

        return ['status' => 'success', 'code' => '200', 'data' => $data];
    }

    /**
     * @param            $data
     * @param int       $code
     * @param array|null $static
     *
     * @return array
     */
    public static function succeededWithDataAndCode($data, $code, array $static = null)
    {
        Yii::$app->response->statusCode = $code;

        if ($static) {
            return ['status' => 'success', 'code' => $code, 'data' => [$data, ['static' => $static]]];
        }

        return ['status' => 'success', 'code' => $code, 'data' => $data];
    }

    /**
     * @param            $data
     * @param array|null $static
     *
     * @return array
     */
    public static function failedWithData($data, array $static = null)
    {
        Yii::$app->response->statusCode = 201;

        if ($static) {
            return ['status' => 'fail', 'code' => '201', 'data' => [$data, ['static' => $static]]];
        }

        return ['status' => 'fail', 'code' => '201', 'data' => $data];
    }

    /**
     * @param array|null $static
     *
     * @return array
     */
    public static function succeededWithoutData(array $static = null)
    {
        Yii::$app->response->statusCode = 200;

        if ($static) {
            return ['status' => 'success', 'code' => '200', 'data' => [['static' => $static]]];
        }

        return ['status' => 'success', 'code' => '200'];
    }

    /**
     * @param array $errors
     *
     * @return array
     */
    public static function validateErrors(array $errors)
    {
        Yii::$app->response->statusCode = 403;
        $fail = ['status' => 'fail', 'code' => 403, 'fail' => []];
        foreach ($errors as $id => $message) {
            $fail['fail'][] = [
                $id => $message[0]
            ];
        }

        return $fail;
    }

    /**
     * @param string $error
     * @param array|null $static
     *
     * @return array
     */
    public static function errorOnSave(string $error, array $static = null)
    {
        Yii::$app->response->statusCode = 500;

        return ['status' => 'error', 'code' => 500, 'error' => $error, 'static' => $static];
    }

    /**
     * @param $request
     * @return array
     */
    public static function methodNotAllowed($request)
    {
        Yii::$app->response->statusCode = 405;

        return ['status' => 'error', 'code' => 405, 'error' => 'Этот метод запроса должен быть: ' . $request];
    }
}