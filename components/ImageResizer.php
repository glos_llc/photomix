<?php
namespace app\components;

/**
 * Class ImageResizer
 * @package app\components\behaviors\file
 */
class ImageResizer
{
    /**
     * @var resource
     */
    public $image;

    /**
     * @var string
     */
    public $imageType;

    /**
     * @var array
     */
    public $imageInfo;

    /**
     * @param $filename
     */
    public function setFile($filename)
    {
        $this->imageInfo = $imageInfo = getimagesize($filename);
        $this->imageType = $imageInfo[2];
        
        if ($this->imageType == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->imageType == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->imageType == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    /**
     * @param $filename
     * @param int $imageType
     * @param int $compression
     * @param null $permissions
     */
    public function save($filename, $imageType = IMAGETYPE_JPEG, $compression = 100, $permissions = null)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($imageType == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($imageType == IMAGETYPE_PNG) {
            imagepng($this->image, $filename);
        }
        if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

    /**
     * @param int $imageType
     */
    public function output($imageType = IMAGETYPE_JPEG)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif($imageType == IMAGETYPE_GIF) {
            imagegif($this->image);
        } elseif($imageType == IMAGETYPE_PNG) {
            imagepng($this->image);
        }
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return imagesx($this->image);
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return imagesy($this->image);
    }

    /**
     * @param $height
     */
    public function resizeToHeight($height)
    {
        if ($height < $this->getHeight()) {
            $ratio = $height / $this->getHeight();
            $width = $this->getWidth() * $ratio;
            $this->resize($width, $height);
        }
    }

    /**
     * @param $width
     */
    public function resizeToWidth($width)
    {
        if ($width < $this->getWidth()) {
            $ratio = $width / $this->getWidth();
            $height = $this->getHeight() * $ratio;
            $this->resize($width, $height);
        }
    }

    /**
     * @param $scale
     */
    public function scale($scale)
    {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getHeight() * $scale / 100;
        $this->resize($width, $height);
    }

    /**
     * @param $width
     * @param $height
     */
    public function resize($width, $height)
    {
        $newImage = imagecreatetruecolor($width, $height);
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;
    }
}
?>